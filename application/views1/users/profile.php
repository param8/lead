<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Profile Detail</h5>
      </div>

      <div class="card-body pt-0">
        <form id="updateProfileForm" method="post" action="<?=base_url('User/update_profile');?>"
          enctype="multipart/form-data">
          <div class="settings-form row">
            <div class="col-md-6">
              <div class="form-group">
                <label>Name <span class="star-red">*</span></label>
                <input type="text" name="name" class="form-control" value="<?=$user->name?>" placeholder="Enter  Name">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Email <span class="star-red">*</span></label>
                <input type="email" name="email" id="email" class="form-control" value="<?=$user->email?>"
                  placeholder="Enter Email" readonly>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Contact Number <span class="star-red">*</span></label>
                <input type="text" class="form-control" name="phone" id="phone" value="<?=$user->phone?>"
                  placeholder="Enter Contact Number" maxlength="10"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label>Alternate Number</label>
                <input type="text" class="form-control" name="alternative_no" id="alternative_no"
                  value="<?=$user->user_alternative_no?>" placeholder="Enter Whatsapp Number" maxlength="10"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <p class="settings-label">Profile Pic </p>
                <div class="settings-btn">
                  <input type="file" accept="image/*" name="image" id="file" onchange="loadFile(event)"
                    value="<?=base_url($user->profile_pic)?>" class="hide-input">
                  <label for="file" class="upload">
                    <i class="feather-upload"></i>
                  </label>
                </div>
                <h6 class="settings-size">Recommended image size is <span>150px x 150px</span></h6>
                <div class="upload-images">
                  <img src="<?=base_url($user->profile_pic)?>" alt="Image">
                  <a href="javascript:void(0);" class="btn-icon logo-hide-btn">
                    <i class="feather-x-circle"></i>
                  </a>
                </div>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Pincode </label>
                <input type="text" class="form-control" name="pincode" id="pincode" minlength="6" maxlength="6"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->pincode?>">
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>State</label>
                <select class="form-control js-example-basic-single" name="state" id="state"
                  onchange="get_city(this.value)">
                  <option value="">Select State</option>
                  <?php foreach($states as $state){?>
                  <option value="<?=$state->id?>" <?=$state->id==$user->state ? 'selected' : ''?>><?=$state->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>City </label>
                <select class="form-control city js-example-basic-single" name="city" id="city">
                  <option value="">Select City</option>
                </select>
              </div>
            </div>


            <div class="col-md-6">
              <div class="form-group">
                <label>Address </label>
                <textarea class="form-control" name="address"><?=$user->address?></textarea>
              </div>
            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label>Gender <span class="text-danger">*</span></label><br>
                <?php $genders = array('Male','Female');
                  foreach($genders as $gender){
                  ?>
                <span><input type="radio" class="" name="gender" value="<?=$gender?>"
                    <?=$gender==$user->gender ? 'checked' : ''?>> <?=$gender?></span>
                <?php
                 }
                ?>
              </div>
            </div>

            <div class="form-group mb-0">
              <div class="settings-btns">
                <button type="submit" class="btn btn-orange">Update</button>
                <button type="reset" class="btn btn-grey">Cancel</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<script>
$("form#updateProfileForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$user->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('profile')?>";

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$(document).ready(function() {
  get_city(<?=$user->state?>, <?=$user->id?>);
});
</script>