<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('User/update')?>" id="userEditForm" method="post">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-xl-6">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">User Basic Detail</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Full Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>" readonly>
              </div>



              <div class="form-group">
                <label>Email Address <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="email" id="email" value="<?=$user->email?>" readonly>
              </div>


              <div class="form-group">
                <label>Contact No. <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->phone?>" readonly>
              </div>

              <div class="form-group">
                <label>Alternative No. </label>
                <input type="text" class="form-control" name="alternative_no" id="alternative_no" minlength="10"
                  maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->user_alternative_no?>" readonly>
              </div>

              <div class="form-group">
                <label>User Role <span class="text-danger">*</span></label>
                <select class="form-control" name="user_type" id="user_type" disabled>

                  <?php foreach($roles as $role){?>
                  <option value="<?=$role->id?>" <?=$page_title== $role->name ? 'selected' : ''?>><?=$role->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>



              <div class="form-group">
                <label>Gender <span class="text-danger">*</span></label><br>
                <?php $genders = array('Male','Female');
                  foreach($genders as $gender){
                  ?>
                <span><input type="radio" class="" name="gender" value="<?=$gender?>"
                    <?=$gender==$user->gender ? 'checked' : ''?> disabled> <?=$gender?></span>
                <?php
                 }
                ?>
                <!-- <span><input type="radio" class="" name="gender" value="Male" checked> Male</span>
                <span class="ml-5"><input type="radio" class="" name="gender" value="Female"> Female</span> -->
              </div>


            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-6 col-xl-6">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">User Location</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Address <span class="text-danger">*</span></label>
                <textarea class="form-control" name="address" id="address" disabled><?=$user->address?></textarea>
              </div>
              <div class="form-group">
                <label>State <span class="text-danger">*</span></label>
                <select class="form-control js-example-basic-single" name="state" id="state"
                  onchange="get_city(this.value)" disabled>
                  <option value="">Select State</option>
                  <?php foreach($states as $state){?>
                  <option value="<?=$state->id?>" <?=$state->id==$user->state ? 'selected' : ''?>><?=$state->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>City <span class="text-danger">*</span></label>
                <select class="form-control city js-example-basic-single" name="city" id="city" disabled>

                  <option value="">Select City</option>

                </select>
              </div>
              <div class="form-group">
                <label>Pincode <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="pincode" id="pincode" minlength="6" maxlength="6"
                  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                  value="<?=$user->pincode?>" disabled>
              </div>
            </div>

          </div>
        </div>
      </div>
  </div>
  </form>
</div>
</div>
</div>
<script>
$(document).ready(function() {
  get_city(<?=$user->state?>, <?=$user->id?>);
});
</script>