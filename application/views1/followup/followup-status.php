<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          
        </div>
      </div>
      
      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
        
              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="text-center text-danger">
                        <?//=$message?>
                      </div>
                      <?php if($this->session->userdata('user_type')!=1 && $permission[8]=='Filter'){?>
              <form method="post" id="filter-data" action="<?=base_url('Followup/setSessionFollowup');?>">
                <div class="row">

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Customers</label><br>
                      <select id="customers_filter" class="form-control js-example-basic-multiple"
                        name="customers_filter" onchange="this.form.submit();">
                        <option value="">Select Customer</option>
                        <?php foreach($customers as $customer){?>
                        <option value="<?=$customer->id?>"
                          <?=($this->session->userdata('customers_filter')==$customer->id)?'selected':'';?>>
                          <?=$customer->name.'('.$customer->phone.')'?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetCustomerFilter()" id="resetCustomerFilter"
                        <?=empty($this->session->userdata('customers_filter')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Months</label>
                      <?php $months = array('0'=>'All Months','01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');?>
                      <select id="monthname_followup" class="form-control" name="monthname_followup"
                        onchange="this.form.submit();">
                        <?php foreach($months as $month_key=>$month){?>
                        <option value="<?=$month_key?>"
                          <?=(!empty($this->session->userdata('monthname_followup')) AND $this->session->userdata('monthname_followup')==$month_key) ? 'selected' :  '' ?>>
                          <?=$month?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetMonthName()" id="resetMonthName"
                        <?=empty($this->session->userdata('monthname_followup')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <?php //if($this->session->userdata('user_type')==2){?>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>Sale Users</label>
                      <select id="assign_user_followup" class="form-control" name="assign_user_followup"
                        onchange="this.form.submit();">
                        <option value="">All Users</option>
                        <?php foreach($users as $user){?>
                        <option value="<?=$user->id?>"
                          <?=$this->session->userdata('assign_user_followup')==$user->id ? 'selected':'' ;?>>
                          <?=$user->name.'('.$user->roleName.')'?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetAssignUser()" id="resetAssignUser"
                        <?=empty($this->session->userdata('assign_user_followup')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <?php //} ?>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>From Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="From Date"
                          name="followup_from_date" id="followup_from_date"
                          value="<?=!empty($this->session->userdata('followup_from_date')) ? $this->session->userdata('followup_from_date'):'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetFromDate()" id="resetFromDate"
                        <?=empty($this->session->userdata('followup_from_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <div class="reset_btn">
                      <label>To Date</label>
                      <div class="input-group">
                        <input type="text" class="form-control" onfocus="(this.type='date')" placeholder="To Date"
                          name="followup_to_date" id="followup_to_date"
                          value="<?=!empty($this->session->userdata('followup_to_date')) ? $this->session->userdata('followup_to_date') :'';?>"
                          onchange="this.form.submit();">
                      </div>
                      <span class="close_button" onclick="resetToDate()" id="resetToDate"
                        <?=empty($this->session->userdata('followup_to_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>

                  <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                    <label>Lead Status</label>
                    <div class="input-group">

                      <select id="lead_status_followup" class="form-control" name="lead_status_followup"
                        onchange="this.form.submit();">
                        <option value="">Select Lead Status</option>
                        <?php foreach($leads as $lead){?>
                        <option value="<?=$lead->id?>"
                          <?=$this->session->userdata('lead_status_followup')==$lead->id ? 'selected':'' ;?>>
                          <?=$lead->name?></option>
                        <?php } ?>
                      </select>
                      <span class="close_button" onclick="resetLeadStatusFollowup()" id="resetleadStatus"
                        <?=empty($this->session->userdata('lead_status_followup')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="url" id="url" value="<?='followup-status/'.$folowup_status?>">
              </form>
              <hr>
              <?php } ?>
                      <?php if($this->session->userdata('user_type')==1){?>
                      <div class="row">
                        <div class="col-sm-6">
                          <select class="form-control" name="followupAdminID" id="followupAdminID"
                            onchange="getFollowupAdminSession(this.value)">
                            <option value="">Select Admin</option>
                            <?php foreach($admins as $admin){?>
                            <option value="<?=$admin->id?>"
                              <?=$admin->id == $this->session->userdata('followupAdminID') ? 'selected' : ''?>>
                              <?=$admin->name?>
                            </option>
                            <?php } ?>
                          </select>

                        </div>

                      </div>
                      <hr>
                      <?php } ?>
                      <div class="table-responsive">
                        <table class=" table table-hover table-center mb-0" id="followupStatusDataTable">
                          <thead>
                            <tr>
                            <th>S.no.</th>
                            <?php if($this->session->userdata('user_type')!=1){?>
                            <th>Action</th>
                            <?php } ?>
                            <th>Client Name</th>
                            <th>Client Phone No.</th>
                            <th>Assign Date</th>
                            <th>Followup Date Time</th>
                            <th>Remark</th>
                            <th>Lead Type</th>
                            <th>Lead Source</th>
                            <th>Lead Status</th>
                            <th>Assign To</th>
                            <th>Assign By</th>
                            <th>Enquery Date</th>
                            </tr>
                          </thead>
                          <tbody>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        


        <script>
        $(document).ready(function() {
          //alert('dfgfgf');
          // $('#example').DataTable();
          // } );
          var dataTable = $('#followupStatusDataTable').DataTable({
            "processing": true,
            "serverSide": true,
            buttons: [{
              extend: 'excelHtml5',
              text: 'Download Excel'
            }],
            "order": [],
            "ajax": {
              url: "<?=base_url('Followup/ajaxFollowupStatus/'.$uri.'/'.$folowup_status)?>",
              type: "POST"
            },
            "columnDefs": [{
              "targets": [0],
              "orderable": false,
            }, ],
          });
        });


        

        function resetCustomerFilter() {
          $.ajax({
            url: '<?=base_url("Followup/resetCustomerFilter")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

      function resetFromDate() {
        $.ajax({
          url: '<?=base_url("Followup/resetFromDateFollowup")?>',
          type: 'POST',
          data: {
            ResetSesession: 'ResetSesession'
          },
          success: function(data) {
            location.reload();
          },
        });
      }

      function resetToDate() {
        $.ajax({
          url: '<?=base_url("Followup/resetToDateFollowup")?>',
          type: 'POST',
          data: {
            ResetSesession: 'ResetSesession'
          },
          success: function(data) {
            location.reload();
          },
        });
      }

      function resetMonthName() {
        $.ajax({
          url: '<?=base_url("Followup/resetMonthNameFollowup")?>',
          type: 'POST',
          data: {
            ResetSesession: 'ResetSesession'
          },
          success: function(data) {
            location.reload();
          },
        });
      }

      function resetAssignUser() {
        $.ajax({
          url: '<?=base_url("Followup/resetAssignUserFollowup")?>',
          type: 'POST',
          data: {
            ResetSesession: 'ResetSesession'
          },
          success: function(data) {
            location.reload();
          },
        });
      }

      function resetLeadStatusFollowup() {
        $.ajax({
          url: '<?=base_url("Followup/resetLeadStatusFollowup")?>',
          type: 'POST',
          data: {
            ResetSesession: 'ResetSesession'
          },
          success: function(data) {
            location.reload();
          },
        });
      }

        function getFollowupAdminSession(id) {
          $.ajax({
            url: "<?=base_url('Ajax_controller/getFollowupAdminSession')?>",
            type: 'POST',
            data: {
              id
            },
            success: function(data) {
              location.reload();
            },

          });
        }

        $('#select-all').click(function(event) {
          if (this.checked) {
            // Iterate each checkbox
            $(':checkbox').each(function() {
              //this.checked = true;  
              var cells = $('#assiginDataTable').dataTable().api().cells().nodes();
              $(cells).find('.chk_cls').prop('checked', this.checked);
            });
          } else {
            $(':checkbox').each(function() {
              this.checked = false;
            });
          }
        });

        function close_modal() {
          $('#assiginEnquiry').modal('hide');
        }

        function assiginEnquiry() {
          $('#assiginEnquiry').modal('show');
        }

        $("form#assiginEnquiryForm").submit(function(e) {
          $(':input[type="submit"]').prop('disabled', true);
          e.preventDefault();
          var formData = new FormData(this);
          $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data) {
              if (data.status == 200) {
                toastNotif({
                  text: data.message,
                  color: '#5bc83f',
                  timeout: 5000,
                  icon: 'valid'
                });
                $(':input[type="submit"]').prop('disabled', false);
                setTimeout(function() {

                  location.href = "<?=base_url('followup')?>";

                }, 1000)

              } else if (data.status == 403) {
                toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });

                $(':input[type="submit"]').prop('disabled', false);
              } else {
                toastNotif({
                  text: data.message,
                  color: '#da4848',
                  timeout: 5000,
                  icon: 'error'
                });
                $(':input[type="submit"]').prop('disabled', false);
              }
            },
            error: function() {}
          });
        });

        function resetCustomerFilter() {
          $.ajax({
            url: '<?=base_url("Followup/resetCustomerFilter")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetFromDate() {
          $.ajax({
            url: '<?=base_url("Followup/resetFromDateFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetToDate() {
          $.ajax({
            url: '<?=base_url("Followup/resetToDateFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetMonthName() {
          $.ajax({
            url: '<?=base_url("Followup/resetMonthNameFollowup")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }
        </script>