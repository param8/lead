<?php 
class Dashboard extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->not_admin_logged_in();
		$this->load->model('auth_model');
		$this->load->model('dashboard_model');
		$this->load->model('user_model');
	}

	public function index()
	{	 

		$data['uri'] ='dashboard';
		$menu = $this->Common_model->get_menu(array('url'=>$data['uri']));
		$menuID = $menu->id;
		$data['permission'] = permissions()->$menuID;

		$data['page_title'] = 'Dashboard';
    $condition = $this->session->userdata('user_type') == 2 ? array('sales.adminID'=>$this->session->userdata('adminID')) : array('sales.adminID'=>$this->session->userdata('adminID'));
	
		$data['sales'] = $this->dashboard_model->get_sale_report($condition);


		$pending_amount = $this->dashboard_model->get_pending_amount($condition);


		//$data['yearly_sales'] = $this->dashboard_model->get_yearly_sale_report($condition);
		$monthly_target =array();
		$users = array();
		if($this->session->userdata('user_type') == 2){
			$monthly_target = $this->user_model->get_users(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')));
			
			$target = 0;
			 foreach($monthly_target as $targets){
				
				$target +=$targets->monthly_target;
				if(!empty($targets->monthly_target)){
					$users[$targets->id] = $targets->name;
				}
				
			 }
     
			 if(!empty($target)){
				$target_month = round(($data['sales']->totalPayments*100)/$target);
			 }else{
				$target_month = 0;
			 }

		}else{
			$monthly_target = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID')));
			$target = $monthly_target->monthly_target;
			if(!empty($target)){
					$target_month = round(($data['sales']->totalPayments*100)/$target);
				}else{
					$target_month = 0;
				 }
				 //$monthly_target[] = array($monthly_target);
				 $users[$monthly_target->id] = $monthly_target->name;
		}

		$data['traget_percentage'] = $target_month;
		$pending_condition = $this->session->userdata('user_type') == 2 ? array('adminID'=>$this->session->userdata('adminID')) : array('adminID'=>$this->session->userdata('adminID'));
		$data['pendin_amount_recieve'] = $this->dashboard_model->get_pending_amount_recieve($pending_condition);

	   $amount_pending_recived = round($data['pendin_amount_recieve']->total_pending_amount);
     
		 $data['pending_amount_percentage'] = 0;

		 if(!empty($amount_pending_recived)){
			$data['pending_amount_percentage'] = ($amount_pending_recived*100)/$pending_amount->totalPending;
		 }
  
		$data['this_month_sale_amount'] = $this->this_month_sale_amount($target,$data['sales'],$data['permission']);

		$data['this_month_recieve_amount'] = $this->this_month_recieve_amount($target,$amount_pending_recived,$data['permission']);

		$data['user_monthly_performance'] = $this->user_monthly_performance($target,$data['sales'],$amount_pending_recived,$data['permission']);

		$data['year_sale_performance'] = $this->year_sale_performance($target,$data['sales'],$amount_pending_recived,$data['permission']);

		$data['user_wise_monthly_performance'] = $this->user_wise_monthly_performance($data['permission']);

		$data['monthly_flow_chart'] = $this->monthly_flow_chart($target,$data['permission']);
    
	  $this->admin_template('dashboard',$data);
		
	}


	public function this_month_sale_amount($target,$sale,$permission){
		$sale_amount = !empty($sale->totalSalesAmount) ? $sale->totalSalesAmount : 0 ;
		$this_month_sale_amount = "['Total Target[₹ $target]',$target],['Sale Amount [₹ $sale_amount]',$sale_amount],";
		return $this_month_sale_amount;
	}


	public function this_month_recieve_amount($target,$recieved_amount,$permission){

		$amount_recieve = !empty($recieved_amount) ? $recieved_amount : 0 ;
		$this_month_recieve_amount = "['Total Target[₹ $target]',$target],['Recieved Amount [₹ $amount_recieve]',$amount_recieve],";
		return  $this_month_recieve_amount;

	}

	public function user_monthly_performance($target,$sale,$recieved_amount,$permission){
		$current_month = strtoupper(date('F'));
		$sale_amount = !empty($sale->totalSalesAmount) ? $sale->totalSalesAmount : 0 ;
		$amount_recieve = !empty($recieved_amount) ? $recieved_amount : 0 ;
		//$amount_recieve = !empty($sale->totalPayments) ? $sale->totalPayments : 0 ;
		$user_monthly_performance[] = "['".$current_month."',$target,$sale_amount,$amount_recieve],";
		return  $user_monthly_performance;
	}

	public function year_sale_performance($target,$sale,$recieved_amount,$permission){
		$cmonth = date('m');
		if($cmonth >= 4)
		{
	
		$year = date('Y');
		}
		else
		{
		
		 $year = date('Y');
		}
		$max_year = $year+1;

		$current_year = $year.' - '.$max_year;

		$data['monthly_sale'] = $this->dashboard_model->get_financial_report_monthly('monthly_sale');
		$data['monthly_payment'] = $this->dashboard_model->get_financial_report_monthly('recievedAmount');
		$financetarget = $target;
		$financesale = $data['monthly_sale'];
		$financepayment = $data['monthly_payment'];
   
		$finance_report = array();

		$monthly_sale =array();
		$payment = 0;
		$monthly_sale_amount = 0;
		$fmonthname = array();

		foreach($financesale as $sale){
		 $monthly_sale_amount +=$sale['saleamount'];
		}

		foreach($financepayment as $paymentsale){
	  	$payment+=$paymentsale['RecieveAmount'];
		}


		$year_sale_performance[] = "['".$current_year."',$target,$monthly_sale_amount,$payment],";
		return  $year_sale_performance;

	}

	public function monthly_flow_chart($target,$permission){
		$month = date('m');
		$mont=array();
		if($month >= 4)
		{
			$y = date('Y');
			$pt = date('Y', strtotime('+1 year'));
			$sfy = $y."-04-01";
			$efy = $pt."-03-31";
		}
		else
		{
			$y = date('Y', strtotime('-1 year'));
			$pt = date('Y');
			$sfy=$y."-04-01";
			$efy =$pt."-03-31";
		}
		//$current= new Datetime(date('m-Y'));
		$data['monthly_sale'] = $this->dashboard_model->get_financial_report_monthly('monthly_sale');
		$data['monthly_payment'] = $this->dashboard_model->get_financial_report_monthly('recievedAmount');
		$start    = new DateTime($sfy);
		$end     = new DateTime($efy);
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);
		//print_r($period);die;
		foreach ($period as $dt) {
		  $mont[]= $dt->format("m-Y");
		}

		$data['monthly_sale'] = $this->dashboard_model->get_financial_report_monthly('monthly_sale');
		$data['monthly_payment'] = $this->dashboard_model->get_financial_report_monthly('recievedAmount');
		$financetarget = $target;
		$financesale = $data['monthly_sale'];
		$financepayment = $data['monthly_payment'];
   
		$finance_report = array();

		$monthly_sale =array();
		$payment = array();
		$monthly_sale_amount =array();
		$fmonthname = array();
		//$months = array('Apr'=>04,'May'=>05,'Jun'=>05,'Jul'=>06,'Aug'=>07,'Sep'=>08,'Oct'=>09);
		foreach($financetarget as $target){
		$target=$target['target'];
		}
		foreach($financesale as $sale){
	
		$monthly_sale_amount[date('m-Y',strtotime($sale['sale_date']))]=$sale['saleamount'];
		$monthly_sale[] = date('m-Y',strtotime($sale['sale_date']));
		}
		//print_r($monthly_sale_amount);die;
			$pay_date = array();
		foreach($financepayment as $paymentsale){
		$payment[date('m-Y',strtotime($paymentsale['created_at']))]=$paymentsale['RecieveAmount'];
		$pay_date[] = date('m-Y',strtotime($paymentsale['created_at']));
		}
		//print_r($payment);die;
		foreach($mont as $monthname){
		//echo '<pre>';
		$teamtargetbymonth = $this->dashboard_model->team_monthly_target($monthname);

		$total_sales = in_array($monthname,$monthly_sale) ? $monthly_sale_amount[$monthname] : 0;
	
		$total_rec = in_array($monthname,$pay_date)?$payment[$monthname]:0;
		$fmonthname = date('M',strtotime('1-'.$monthname));
		$finance_report[]= '["'.$fmonthname.'", '.$teamtargetbymonth.','. $total_sales.', '.$total_rec.'],';
		}

  return  $monthly_flow_chart = $finance_report;

		//print_r($monthly_flow_chart);die;

	}

	public function user_wise_monthly_performance($permission){

		$role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));

		$monthly_target =array();
		$users = array();
		if($this->session->userdata('user_type') == 2 OR count($usersID) > 1){
			$monthly_target = $this->user_model->get_users(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')));
			
			$target = 0;
			 foreach($monthly_target as $targets){
				
				$target +=$targets->monthly_target;
				if(!empty($targets->monthly_target)){
					$users[$targets->id] = $targets->name;
				}
				
			 }
     
			 //if(!empty($target)){
				$target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;
			//  }else{
			// 	$target_month = 0;
			//  }

		}
		// elseif(count($usersID) > 1){
		// 	$monthly_target = $this->user_model->get_users(array('users.status'=>1,'users.adminID'=>$this->session->userdata('adminID')));
		// 	$target = 0;
		// 	 foreach($monthly_target as $targets){
		// 		$target +=$targets->monthly_target;
		// 		if(!empty($targets->monthly_target)){
		// 			$users[$targets->id] = $targets->name;
		// 		}
		// 	 }
     
		// 	 $target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;

		// }
		else{
			$monthly_target = $this->user_model->get_user(array('users.id'=>$this->session->userdata('id'),'users.adminID'=>$this->session->userdata('adminID')));
			$target = $monthly_target->monthly_target;
			$target_month = !empty($data['sales']->totalPayments) ? round(($data['sales']->totalPayments*100)/$target) : 0;
			// if(!empty($target)){
			// 		$target_month = round(($data['sales']->totalPayments*100)/$target);
			// 	}else{
			// 		$target_month = 0;
			// 	 }
				 //$monthly_target[] = array($monthly_target);
				 $users[$monthly_target->id] = $monthly_target->name;
		}

	
		$condition = $this->session->userdata('user_type') == 2 ? array('sales.adminID'=>$this->session->userdata('adminID')) :array('sales.adminID'=>$this->session->userdata('adminID'));
	
		$sales = $this->dashboard_model->get_sale_reports_user_wise($condition);
  //echo "<pre>";
		//print_r($sales);die;

		$pending_condition = $this->session->userdata('user_type') == 2 ? array('payment_history.adminID'=>$this->session->userdata('adminID')) : array('adminID'=>$this->session->userdata('adminID'));
		$recieved_amount = $this->dashboard_model->get_pending_amount_recieve($pending_condition);
		$sale_amount=array();
		$amount_recieve =array();
    // echo "<pre>";
		// print_r($sales);die;
		foreach($sales as $sale){
			$sale_amount[$sale->user_id] += !empty($sale->totalSales) ? $sale->totalSales : 0 ;
			$amount_recieve[$sale->user_id] += !empty($sale->totalPayments) ? $sale->totalPayments : 0 ;
			//$amount_recieve[$sale->id] = !empty($recieved_amount->total_pending_amount) ? $recieved_amount->total_pending_amount : 0 ;
		}

	 //	print_r($sale_amount);die;
  // //print_r($users);die;
		foreach($users as $key=>$sale){

			$monthly_target = $this->user_model->get_user(array('users.id'=>$key,'users.adminID'=>$this->session->userdata('adminID')));
			$target = $monthly_target->monthly_target;
		
			$link = base_url('payment-history');
			$user_wise_monthly_performance[] = "['$sale', $target, $sale_amount[$key],$amount_recieve[$key],'$link'],";
		}
		return  $user_wise_monthly_performance;
	}


}