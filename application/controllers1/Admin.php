<?php 
class Admin extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();


		$this->load->model('user_model');
	}

	public function index()
	{
		$this->not_admin_logged_in();
    $data['page_title'] = 'Admin';
	  $this->admin_template('users/admin',$data);
	}

	public function ajaxAdmin(){
		$this->not_admin_logged_in();
		$condition = array('users.status'=>1,'users.user_type'=>'2');
		$admins = $this->user_model->make_datatables($condition); // this will call modal function for fetching data
		$data = array();
		//print_r($admins);die;
		foreach($admins as $key=>$admin) // Loop over the data fetched and store them in array
		{
			$button = '';
			$sub_array = array();
			
			$button .= '<a href="'.base_url('admin-site-setting/'.base64_encode($admin['id'])).'" data-bs-toggle="tooltip" data-bs-replacement="bottom" title="Site Setting Detail" class="btn  btn-sm  btn-success">Site Setting</a>';
			$button .= '<a href="'.base_url('view-admin/'.base64_encode($admin['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="View Admin Detail" class="btn  btn-sm  text-warning"><i class="fa fa-eye"></i></a>';
			$button .= '<a href="'.base_url('edit-admin/'.base64_encode($admin['id'])).'" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit Admin Detail" class="btn  btn-sm  text-primary"><i class="fa fa-edit"></i> </a>';
      		$button .= '<a href="javascript:void(0)" onclick="delete_user('.$admin['id'].')" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete Admin" class="btn  btn-sm  text-danger"><i class="fa fa-trash"></i> </a>';
			$profile_pic = $admin['profile_pic'];
			$sub_array[] = $key+1;
			$sub_array[] = $button;
			$sub_array[] = '<img src="'.base_url($profile_pic).'" height="50" width="50">';
			$sub_array[] = $admin['name'];
			$sub_array[] = $admin['phone'];
			$sub_array[] = $admin['email'];
			$sub_array[] = $admin['address'];
			$sub_array[] = $admin['cityName'];
			$sub_array[] = $admin['stateName'];
			$sub_array[] = !empty($admin['pincode'])?$admin['pincode']:'-';
			$sub_array[] = date('d-m-Y', strtotime($admin['created_at']));
		
		  $data[] = $sub_array;
		}
	
		$output = array(
			"draw"                    =>     intval($_POST["draw"]),
			"recordsTotal"            =>     $this->user_model->get_all_data($condition),
			"recordsFiltered"         =>     $this->user_model->get_filtered_data($condition),
			"data"                    =>     $data
		);
		
		echo json_encode($output);
	  }

		public function create(){
			$data['page_title'] = 'Admin';
			$this->admin_template('users/create-admin',$data);
		}

		public function store(){
			
			$name = $this->input->post('name');
			$site_name = $this->input->post('site_name');
			$email = $this->input->post('email');
			$site_email = $this->input->post('site_email');
			$phone = $this->input->post('phone');
			$alternative_no = $this->input->post('alternative_no');
			$user_type = $this->input->post('user_type');
			$profile_pic = 'public/dummy_user.png';	
			$created_by = $this->input->post('created_by');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$address = $this->input->post('address');
			$pincode = $this->input->post('pincode');
			$gender = $this->input->post('gender');
			$password = $this->input->post('password');
			$user_permission = $this->input->post('user_permission');
			$lead_permission = $this->input->post('lead_permission');
			// permission start
			$validate_email = $this->user_model->get_user(array('users.email' => $email));
		  $validate_phone = $this->user_model->get_user(array('users.phone' => $phone));
			$menus = $this->menus();
			$permission = array();
				foreach($menus as $menu){
					if($menu['parent_menu']=='Dashboard'){
						$permission[$menu['id']] = array('View','Add','Edit','Delete','Upload','Export');
					}else{
					$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
					$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
					$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
					$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
					$upload = !empty($this->input->post('upload')[$menu['id']]) ? $this->input->post('upload')[$menu['id']] : '';
					$export = !empty($this->input->post('export')[$menu['id']]) ? $this->input->post('export')[$menu['id']] : '';
					$permission[$menu['id']] = array($view,$add,$edit,$delete,$upload,$export);
					}
				}
				
			$permission_json = json_encode($permission);
				
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}
			if(empty($email)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your email ']); 	
				exit();
			}
			
			if($validate_email){
				echo json_encode(['status'=>403, 'message'=>'This email address is already in use.']); 	
				exit();
			}
	
			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
				exit();
			}
		 
			if($validate_phone){
				echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
				exit();
			}
	
			if(empty($user_type)){
				echo json_encode(['status'=>403, 'message'=>'Please select role ']); 	
				exit();
			}

			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
				exit();
			}
				
				if(empty($state)){
					echo json_encode(['status'=>403, 'message'=>'Please select state ']); 	
					exit();
				}
				if(empty($city)){
					echo json_encode(['status'=>403, 'message'=>'Please select city ']); 	
					exit();
				}
				
		
				if(empty($pincode)){
					echo json_encode(['status'=>403, 'message'=>'Please enter pincode']);  	
					exit();
				}

				if(empty($password)){
					echo json_encode(['status'=>403, 'message'=>'Please enter password']);  	
					exit();
				}

				// if($user_permission!=){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter user permission']);  	
				// 	exit();
				// }

				// if(empty($lead_permission)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter lead permission']);  	
				// 	exit();
				// }

			$unique_id =  uniqid(date('dmYHis'));
			$data = array(	
				'unique_id'   => $unique_id,
				'name'        => $name,
				'email'       => $email,
				'phone'       => $phone,
				'address'     => $address,
				'city'        => $city,
				'state'       => $state,
				'pincode'     => $pincode,
				'profile_pic' => $profile_pic,
				'user_type'   => $user_type,
				'password'    => md5($password),
				'permission'  => $permission_json,
				'status'      => 1,
			);
	
			$user_id = $this->user_model->store_user($data);
			if($user_id){
				$user_detail = array(
					'userID'          => $user_id,
					'alternative_no'  => $alternative_no,
					'gender'          => $gender,
					'user_permission' => $user_permission,
					'lead_permission' => $lead_permission,
				);
			 $store_user_detail = $this->user_model->store_user_detail($user_detail);
			 
			 $site_info = array(
				'adminID'          => $user_id,
				'site_name'  => $site_name,
				'short_name'          => $site_name,
				'site_email' => $site_email,
				'site_contact' => $alternative_no,
				'site_address' => $address
			);
		 $store_site_info = $this->user_model->store_site_info($site_info);

		 $update_admin = array(
			'adminID' =>$user_id
		 );
		 $update = $this->user_model->update_user($update_admin,array('id'=>$user_id));
			 echo json_encode(['status'=>200, 'message'=>'Admin Added Successfully']);   
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
			}
	
		}
		
	public function delete(){
		$id = $this->input->post("id");
		$update = $this->user_model->update_user(array('status'=>0),$id);
		if($update){
			echo json_encode(['status'=>200, 'message'=>'Admin Deleted Successfully']);   
		}else{
			echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
		}
		
	}


	public function siteSetting(){
		$id = base64_decode($this->uri->segment(2));
		$data['site_info'] = $this->user_model->get_site_info(array('adminID'=>$id));		
		//print_r($data);die;
		$this->admin_template('users/site-setting',$data);
	}

		public function edit(){
			 $data['page_title'] = 'Edit Admin';
			 $id = base64_decode($this->uri->segment(2));
			$data['user'] = $this->user_model->get_user(array('users.id'=>$id));
			$data['menus'] = $this->Common_model->get_menues(array('status'=>1));
			$this->admin_template('users/edit-admin',$data);
		}

		public function view(){
			$data['page_title'] = 'View Admin';
			$id = base64_decode($this->uri->segment(2));
		 $data['user'] = $this->user_model->get_user(array('users.id'=>$id));
		 $data['menus'] = $this->Common_model->get_menues(array('status'=>1));
		 $data['website_setting'] = $this->Common_model->get_site_info(array('adminID'=>$id));
		 $this->admin_template('users/view-admin',$data);
	 }


		public function update(){
			$id = $this->input->post('id');
			$name = $this->input->post('name');
			$site_name = $this->input->post('site_name');
			$email = $this->input->post('email');
			$site_email = $this->input->post('site_email');
			$phone = $this->input->post('phone');
			$alternative_no = $this->input->post('alternative_no');
			$user_type = $this->input->post('user_type');
			$profile_pic = 'public/dummy_user.png';	
			$created_by = $this->input->post('created_by');
			$state = $this->input->post('state');
			$city = $this->input->post('city');
			$address = $this->input->post('address');
			$pincode = $this->input->post('pincode');
			$gender = $this->input->post('gender');
			$password = $this->input->post('password');
			$user_permission = $this->input->post('user_permission');
			$lead_permission = $this->input->post('lead_permission');
			// permission start
			$validate_email = $this->user_model->get_user(array('users.email' => $email,'users.id<>'=>$id));
		  $validate_phone = $this->user_model->get_user(array('users.phone' => $phone ,'users.id<>'=>$id));
			$menus = $this->menus();
			$permission = array();
				foreach($menus as $menu){
					if($menu['parent_menu']=='Dashboard'){
						$permission[$menu['id']] = array('View','Add','Edit','Delete','Upload','Export');
					}else{
					$view = !empty($this->input->post('view')[$menu['id']]) ? $this->input->post('view')[$menu['id']] : '';
					$add = !empty($this->input->post('add')[$menu['id']]) ? $this->input->post('add')[$menu['id']] : '';
					$edit = !empty($this->input->post('edit')[$menu['id']]) ? $this->input->post('edit')[$menu['id']] : '';
					$delete = !empty($this->input->post('delete')[$menu['id']]) ? $this->input->post('delete')[$menu['id']] : '';
					$upload = !empty($this->input->post('upload')[$menu['id']]) ? $this->input->post('upload')[$menu['id']] : '';
					$export = !empty($this->input->post('export')[$menu['id']]) ? $this->input->post('export')[$menu['id']] : '';
					$permission[$menu['id']] = array($view,$add,$edit,$delete,$upload,$export);
					}
				}
				
			$permission_json = json_encode($permission);
				
		 if(empty($name)){
				echo json_encode(['status'=>403, 'message'=>'Please enter name']); 	
				exit();
			}
			if(empty($email)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your email ']); 	
				exit();
			}
			
			if($validate_email){
				echo json_encode(['status'=>403, 'message'=>'This email address is already in use.']); 	
				exit();
			}
	
			if(empty($phone)){
				echo json_encode(['status'=>403, 'message'=>'Please enter your phone ']); 	
				exit();
			}
		 
			if($validate_phone){
				echo json_encode(['status'=>403, 'message'=>'This phone number is already in use.']); 	
				exit();
			}
	
			

			if(empty($address)){
				echo json_encode(['status'=>403, 'message'=>'Please enter address ']); 	
				exit();
			}
				
				if(empty($state)){
					echo json_encode(['status'=>403, 'message'=>'Please select state ']); 	
					exit();
				}
				if(empty($city)){
					echo json_encode(['status'=>403, 'message'=>'Please select city ']); 	
					exit();
				}
				
		
				if(empty($pincode)){
					echo json_encode(['status'=>403, 'message'=>'Please enter pincode']);  	
					exit();
				}

				

				// if(empty($user_permission)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter user permission']);  	
				// 	exit();
				// }

				// if(empty($lead_permission)){
				// 	echo json_encode(['status'=>403, 'message'=>'Please enter lead permission']);  	
				// 	exit();
				// }

			$unique_id =  uniqid(date('dmYHis'));
			$data = array(	
				'name'        => $name,
				'email'       => $email,
				'phone'       => $phone,
				'address'     => $address,
				'city'        => $city,
				'state'       => $state,
				'pincode'     => $pincode,
				'permission'  => $permission_json,
			);
	
			$update = $this->user_model->update_user($data,array('id'=>$id));
			if($update){
				$user_detail = array(
					'alternative_no'  => $alternative_no,
					'gender'          => $gender,
					'user_permission' => $user_permission,
					'lead_permission' => $lead_permission,
				);
			 $update_user_detail = $this->user_model->update_user_detail($user_detail,$id);
			 
			 
			 echo json_encode(['status'=>200, 'message'=>'Admin updated Successfully']);   
			}else{
				echo json_encode(['status'=>403, 'message'=>mysqli_error()]);
			}
	
		}

	 public function profile(){
		$this->not_logged_in();
		$data['page_title'] = 'Profile';
		$data['panel'] = 'Panel';
		$id = $this->session->userdata('id');
		$data['user'] =$this->user_model->get_user(array('users.id'=>$id));
		$data['states'] = $this->Common_model->get_states(array('country_id'=>101));
		$this->template('web/panel/profile',$data);
	 }

	 
		public function reset_password(){
			$this->not_logged_in();
			$data['page_title'] = 'Profile';
			$data['panel'] = 'Panel';
			$this->template('web/panel/reset-password',$data);
		}

		public function update_password(){
			$userID = $this->session->userdata('id');
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$user = $this->user_model->get_user(array('users.id' =>$userID));

			if(empty($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter old password']);  	
				exit();
			}

			if($user->password != md5($old_password)){
				echo json_encode(['status'=>403, 'message'=>'Old password is incorrect']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}


			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}

		public function forget_password()
		{
			$data['page_title'] = 'Forget Password';	
		  $this->template('web/forget-password',$data);
		}


		public function change_password(){
			$data['page_title'] = 'Change Password';	
		  $this->template('web/change-password',$data);
		}

		public function password_change(){
			$verify_otp = $this->session->userdata('verify_otp');
			$detail = $this->session->userdata('detail');
			$otp = $this->input->post('otp');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');
      
			$users = $this->user_model->get_user_detail($detail);

			$userID = $users->row()->id;

			if(empty($otp)){
				echo json_encode(['status'=>403, 'message'=>'Please enter  OTP']);  	
				exit();
			}

			if($verify_otp != $otp){
				echo json_encode(['status'=>403, 'message'=>'Please enter valid OTP']);  	
				exit();
			}

			if(empty($new_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter new password']);  	
				exit();
			}

			if(strlen($new_password)<6){
				echo json_encode(['status'=>403, 'message'=>'Password minimum length must be at least 6 characters']);  	
				exit();
			}

			if(empty($confirm_password)){
				echo json_encode(['status'=>403, 'message'=>'Please enter confirm password']);  	
				exit();
			}

			if($new_password != $confirm_password){
				echo json_encode(['status'=>403, 'message'=>'New Password and Confirm Password must be the same']);  	
				exit();
			}

			$data = array(
				'password' => md5($new_password)
			);
   
			$update = $this->user_model->update_user($data,$userID);
			if($update){
		  	echo json_encode(['status'=>200, 'message'=>'Password updated successfully please login again !..']); 
			}else{
				echo json_encode(['status'=>302, 'message'=>'something wrong happened']);   
			}
		}
	
}