<?php 

class User_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName,role.other_permission as roleOtherPermission');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->join('role','role.id = users.user_type','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('users.email', $_POST["search"]["value"]);
    $this->db->or_like('users.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('users.id','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName,role.other_permission as roleOtherPermission');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->join('role','role.id = users.user_type','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('users.email', $_POST["search"]["value"]);
    $this->db->or_like('users.phone', $_POST["search"]["value"]);
    $this->db->or_like('cities.name', $_POST["search"]["value"]);
    $this->db->or_like('states.name', $_POST["search"]["value"]);
   }
   $this->db->order_by('users.id','desc');
	   return $this->db->count_all_results();
  }

 

  public function get_user($condition){
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName,role.other_permission as roleOtherPermission');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->join('role','role.id = users.user_type','left');
	$this->db->where($condition);
	return $this->db->get()->row();
  }
  public function get_users($condition){
    
    $role = role();
    $role_permission = json_decode($role->other_permission);
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName,role.other_permission as roleOtherPermission');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->join('role','role.id = users.user_type','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
			if($this->session->userdata('user_type')!=1){
        if($this->session->userdata('user_type')!=5){
      if(!empty($usersID)){
        $this->db->where_in('users.id',$usersID );
      }
    }
		}
    }
		
	  return $this->db->get()->result();
	// echo $this->db->last_query();
  }

  public function get_task_users($condition){
    $role = role();
    $role_permission = json_decode($role->other_permission);
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName,role.other_permission as roleOtherPermission');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->join('role','role.id = users.user_type','left');
    $this->db->where($condition);
   
	 return $this->db->get()->result();


  }

  public function get_users_in_condition($condition,$users){
    $this->db->select('users.*,cities.name as cityName,states.name as stateName,user_detail.alternative_no as user_alternative_no,user_detail.dob,user_detail.gender,user_detail.monthly_target,user_detail.monthly_target_date,user_detail.user_permission,user_detail.lead_permission,role.name as roleName');
    $this->db->from('users');
    $this->db->join('cities','cities.id = users.city','left');
    $this->db->join('states','states.id = users.state','left'); 
    $this->db->join('user_detail','user_detail.userID = users.id','left');
    $this->db->join('role','role.id = users.user_type','left');
	  $this->db->where($condition);
    $this->db->where_in('users.id',$users);
	return $this->db->get()->result();
  }

  public function store_user($data){
	 $this->db->insert('users',$data);
   return $this->db->insert_id();
  }


  public function update_user($data,$condition){
	$this->db->where($condition);
	return $this->db->update('users',$data);
  }

  public function store_user_detail($data){
    return $this->db->insert('user_detail',$data);
  }

  public function get_site_info($condition){
    $this->db->select('site_info.*');
    $this->db->from('site_info');
	  $this->db->where($condition);
	  return $this->db->get()->row();
  }

  public function store_site_info($data){
    return $this->db->insert('site_info',$data);
  }

  public function update_user_detail($data,$userID){
    $this->db->where('userID',$userID);
	return $this->db->update('user_detail',$data);
	 //echo $this->db->last_query();
	  
  }

  public function get_user_detail($detail){

    $this->db->where('email',$detail);
    $this->db->or_where('phone',$detail);
   return $this->db->get('users');

  }

  public function store_monthly_target($data){
   return $this->db->insert('monthly_target_history',$data);
  }

  
}