<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission=='Add'){?>
          <!-- <div class="col-sm-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="<?//=base_url('create-customer')?>"
                style="float: right">Create <?//=$page_title?></a>
            </div>
          </div> -->
          <?php } ?>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="text-center text-danger">
                <?//=$message?>
              </div>
              <?php if($this->session->userdata('user_type')==1){?>
              <div class="row">
                <div class="col-sm-6">
                  <select class="form-control" name="serviceAdminID" id="serviceAdminID"
                    onchange="getServiceAdminSession(this.value)">
                    <option value="">Select Admin</option>
                    <?php foreach($admins as $admin){?>
                    <option value="<?=$admin->id?>"
                      <?=$admin->id == $this->session->userdata('serviceAdminID') ? 'selected' : ''?>><?=$admin->name?>
                    </option>
                    <?php } ?>
                  </select>

                </div>

              </div>
              <hr>
              <?php } 
              ?>
              <?php if($permission[8]=='Filter'){?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-body">
                      <form method="post" id="filter-data" action="<?=base_url('Services/setSession');?>">
                        <div class="row">
                          <div class="col-md-4 col-lg-4 col-xl-4 col-sm-6">
                            <div class="reset_btn">
                              <label>Users</label>
                              <select id="service_user" class="form-control" name="service_user"
                                onchange="this.form.submit();">
                                <option value="">All Users</option>
                                <?php foreach($users as $user){?>
                                <option value="<?=$user->id?>"
                                  <?=$this->session->userdata('service_user')==$user->id ? 'selected':'' ;?>>
                                  <?=$user->name.'('.$user->roleName.')'?></option>
                                <?php } ?>
                              </select>
                              <span class="close_button" onclick="resetServiceUser()" id="resetServiceUser"
                                <?=empty($this->session->userdata('service_user')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>

                          <div class="col-md-4 col-lg-4 col-xl-4 col-sm-6">
                            <div class="reset_btn">
                              <label>Service Status</label>
                              <?php $service_status = array('New','Cancled');?>
                              <select id="service_status" class="form-control" name="service_status"
                                onchange="this.form.submit();">

                                <option value="">Service Status</option>
                                <?php foreach($service_status as $service_statu){?>
                                <option value="<?=$service_statu?>"
                                  <?=$this->session->userdata('service_status')==$service_statu ? 'selected':'' ;?>>
                                  <?=$service_statu?></option>
                                <?php } ?>
                              </select>
                              <span class="close_button" onclick="resetServiceStatus()" id="resetServiceStatus"
                                <?=empty($this->session->userdata('service_status')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>

                        </div>
                      </form>
                      <hr>
                      <?php } ?>

                      <div class="table-responsive">
                        <table class=" table table-hover table-center mb-0" id="serviceRenewalDataTable">
                          <thead>
                            <tr>
                              <th>S.no.</th>
                              <th>User Name</th>
                              <th>Domain / Url</th>
                              <th>Product</th>
                              <th>Expiry Date</th>
                              <th>Status</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php 
                     $sno = 1;
                    foreach($services as $key=>$renew){?>
                            <tr>
                              <td><?=$sno++?></td>
                              <td><?=$renew[1]?></td>
                              <td><?=$renew[2]?></td>
                              <td><?=$renew[3]?></td>
                              <td><?=$renew[4]?></td>
                              <td><?=$renew[5]?></td>
                              <td><?=$renew[6]?></td>
                            </tr>
                            <?php } ?>

                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="modal fade" id="servicesModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
          aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-wrench text-warning"></i>
                  <?=$page_title?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="close_modal()">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="viewServices_detail" style="background-color:#f8f9fa">

              </div>
              <div class="modal-footer">
                <button type="button" class="btn  btn-outline-danger " onclick="close_modal()" data-dismiss="modal"
                  aria-label="Close">Close <i class="fa fa-close"></i></button>
              </div>

            </div>
          </div>
        </div>


        <script>
        $(document).ready(function() {
          var dataTable = $('#serviceRenewalDataTable').DataTable({
            "processing": true,
            "serverSide": false,
            buttons: [{
              extend: 'excelHtml5',
              text: 'Download Excel'
            }],
            "order": [],

            "columnDefs": [{
              "targets": [0],
              "orderable": true,
            }, ],
          });
        });




        function getServiceAdminSession(id) {
          $.ajax({
            url: "<?=base_url('Ajax_controller/getServiceAdminSession')?>",
            type: 'POST',
            data: {
              id
            },
            success: function(data) {
              location.reload();
            },

          });
        }

        function view_services(id) {
          $.ajax({
            url: '<?=base_url('Services/get_renew_service_detail')?>',
            type: 'post',
            data: {
              id
            },
            dataType: 'html',
            success: function(response) {
              $('#servicesModel').modal('show');
              $('#viewServices_detail').html(response);
              togglePay();
            }
          });
        }

        function close_modal() {
          $('#servicesModel').modal('hide');
        }

        function resetServiceUser() {
          $.ajax({
            url: '<?=base_url("Services/resetServiceUser")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function resetServiceStatus() {
          $.ajax({
            url: '<?=base_url("Services/resetServiceStatus")?>',
            type: 'POST',
            data: {
              ResetSesession: 'ResetSesession'
            },
            success: function(data) {
              location.reload();
            },
          });
        }

        function cancle_service(id, service_type) {

          Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, cancle it!'
          }).then((result) => {
            if (result.value) {
              $.ajax({
                url: '<?=base_url('Services/cancle_service')?>',
                type: 'POST',
                data: {
                  id,
                  service_type
                },
                dataType: 'json',
                success: function(data) {
                  if (data.status == 200) {
                    toastNotif({
                      text: data.message,
                      color: '#5bc83f',
                      timeout: 5000,
                      icon: 'valid'
                    });
                    setTimeout(function() {

                      location.href = "<?=base_url('services-renewal')?>"

                    }, 1000)


                  } else if (data.status == 302) {
                    toastNotif({
                      text: data.message,
                      color: '#da4848',
                      timeout: 5000,
                      icon: 'error'
                    });

                  }
                }
              })
            }
          })
        }
        </script>