<link rel="stylesheet"
  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
<script type="text/javascript"
  src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.min.js"></script>
<style>
.border-bottom {
  border-bottom: 1px solid gainsboro;
}

.service-list label {
  cursor: pointer;
  padding: 0 103px 0px 5px;
}


#domainform,
#emailform,
#hostingform,
#sslform,
#softwareform,
#websiteform,
#whatsappform,
#smsform,
#seoform,
#smoform,
#smmform,
#ppcform,
#otherform {
  display: none;
}

.service-list>li:hover {
  cursor: pointer;
  background: #284b895c;
}

.service-list {
  display: none;
  width: 100%;
  position: inherit;
  font-size: 13px;
  border-radius: 2px;
  -webkit-box-shadow: 0 2px 6px rgb(0 0 0 / 10%);
  box-shadow: 0 2px 6px rgb(0 0 0 / 10%);
  border: 1px solid rgba(0, 0, 0, .1);
}

.service-list>li {
  margin: 0;
  height: 100%;
  cursor: pointer;
  font-weight: 400;
  padding: 3px 20px 3px 30px;
  border-bottom: 1px solid lightgray;
}

#select_all {}

body {
  color: #6a6c6f;
  background-color: #f1f3f6;
  margin-top: 30px;
}

.container {
  max-width: 960px;
}

.table>tbody>tr.active>td,
.table>tbody>tr.active>th,
.table>tbody>tr>td.active,
.table>tbody>tr>th.active,
.table>tfoot>tr.active>td,
.table>tfoot>tr.active>th,
.table>tfoot>tr>td.active,
.table>tfoot>tr>th.active,
.table>thead>tr.active>td,
.table>thead>tr.active>th,
.table>thead>tr>td.active,
.table>thead>tr>th.active {
  background-color: #fff;
}

.table-bordered>tbody>tr>td,
.table-bordered>tbody>tr>th,
.table-bordered>tfoot>tr>td,
.table-bordered>tfoot>tr>th,
.table-bordered>thead>tr>td,
.table-bordered>thead>tr>th {
  border-color: #e4e5e7;
}

.table tr.header {
  font-weight: bold;
  background-color: #fff;
  cursor: pointer;
  -webkit-user-select: none;
  /* Chrome all / Safari all */
  -moz-user-select: none;
  /* Firefox all */
  -ms-user-select: none;
  /* IE 10+ */
  user-select: none;
  /* Likely future */
}

.table tr:not(.header) {
  display: none;
}

.table .header td:after {
  opacity: 0;
  content: "\002b";
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  float: right;
  color: #999;
  text-align: center;
  padding: 3px;
  transition: transform .25s linear;
  -webkit-transition: -webkit-transform .25s linear;
}

.table .header.active td:after {
  content: "\2212";
}

.btn-group {
  width: 100%;
}

.btn-group button {
  width: 100%;
}

/*ul{*/
/*  text-decoration: underline;*/
/*}*/
.thead-dark tr {
  background: #ff4300cc;
  color: #fff;
}

.modal-dialog {
  width: 850px;
}

.modal-content {
  border-radius: 0px;
}

ul label {
  font-weight: bold;
}

h2.h4.bg-primary {
  padding: 5px;
}
</style>
<?php 

$service_type_array = array();
$service_detail = array();
 foreach(json_decode($service->service_detail) as $service_type_key=>$detail){

  $service_type_array[] = $service_type_key;
  $service_detail[$service_type_key] = $detail;
 }
//  echo "<pre>";
//  print_r($service_detail);die;
?>
<div class="content">
  <div class="page-wrapper">
    <div class="content container-fluid">
      <div class="page-header">
        <div class="row">
          <div class="col">
            <h3 class="page-title">Create <?=$page_title?></h3>
            <ul class="breadcrumb">
              <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
              <li class="breadcrumb-item active">Create <?=$page_title?></li>
            </ul>
          </div>
        </div>
      </div>
      <form action="<?=base_url('Services/update')?>" id="serviceEditForm" method="post">
        <div class="row">
          <div class="col-md-12 col-lg-12 col-xl-12">
            <div class="card">
              <div class="card-header">
                <h4 class="card-title">Enter <?=$page_title?> Detail</h4>
              </div>
              <div class="card-body">
                <div class="row">
                  <input type="hidden" name="sale_id" id="sale_id" value="<?=$saleID?>">
                  <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="form-group">
                      <label>Services </label>
                      <button type="button" class="form-control btn btn-default" id="show-services"><strong>Select
                          Services</strong> <b class="caret"></b></button>
                      <ul class="service-list" id="service-list" style="list-style-type:none;padding-left: 0;">
                        <!-- € -->
                        <?php foreach($service_types as $service_type){?>
                        <li>
                          <input type="checkbox" class="mt-2" id="<?=$service_type->slug?>" name="services[]"
                            value="<?=$service_type->name?>"
                            <?=in_array($service_type->name,$service_type_array) ? 'checked' : ''?>><label
                            for="<?=$service_type->slug?>"><?=$service_type->name?></label>
                        </li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>


                  <div class="text-right" style="margin: 0 10px 5px 0;">

                    <?php if(!empty($row)){
                      //print_r($row);die;?>
                    <a class="btn btn-primary  btn-rounded" data-toggle="modal"
                      onclick="viewservices(<?php echo $row->enq_id?>)" data-target="#detailModal"><i
                        class="fa fa-eye"></i>View Last Purchase Detail</a>

                    <div class="modal fade" tabindex="-1" role="dialog" id="detailModal">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title text-center"><strong>Previous Purchase Data</strong></h4>
                          </div>

                          <div class="modal-body">
                            <div class="row" id="viewServices_detail"
                              style="display: flex; width: 100%; flex-wrap: wrap;text-align: left;">

                            </div>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div>
                    <?php } ?>
                  </div>
                  <section id="domainform">

                    <div id="domainTtype" class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Domain</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Domain Name</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="domain" name="domain"
                            value="<?=$service_detail['Domain']->domain_name?>" placeholder="Enter Domain Name">
                        </div>
                      </div>
                      <?php// print_r($row['domain_name']);die;?>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="booking_date"><strong>Booking Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="booking_date" name="booking_date"
                            value="<?=$service_detail['Domain']->domain_booking_date?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Years</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeypress='validate(event)' autocomplete="off"
                            id="year" name="year" value="<?=$service_detail['Domain']->domain_year?>"
                            placeholder="Enter Year">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="rDate"><strong>Renewal Date</strong></label>
                          <?php $StaringDate = strtotime(date("Y-m-d"));$newEndingDate =strtotime( "+ 1 year",$StaringDate);?>
                          <input type="date" class="form-control" id="rDate" onkeyup="domainprice()" name="rDate"
                            value="<?=$service_detail['Domain']->doamin_renew_date?>" readonly>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="price"><strong>Price/Year</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" autocomplete="off" id="price" onkeyup="domainprice()"
                            name="price" placeholder="Enter Price"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Domain']->doamin_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="price"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" autocomplete="off" id="dtotalprice" name="dtotalprice"
                            placeholder="Total Price" value="<?=$service_detail['Domain']->domain_total_price?>"
                            readonly>
                        </div>
                      </div>


                    </div>
                  </section>
                  <section id="emailform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Email</h2>
                      <div class="col-md-3 col-lg-3 col-xl-3" id="email-domain">
                        <div class="form-group">
                          <label for="emailDomain"><strong>Domain Name</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" name="emailDomain" id="emailDomain"
                            placeholder="Enter Domain Name" value="<?=$service_detail['Email']->email_domain_name?>">
                        </div>
                      </div>
                      <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="form-group">
                          <label for="sp"><strong>Service Provider</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <select name="emailservice" id="emailservice" class="form-control">
                            <option disabled selected>Select</option>
                            <?php
                                      $serviceproviders = array('Rediffmail PRO','Rediff Enterprises Solution','Google Workspace','Netcore Solution','Microsoft');
                                  //print_r($serviceprovider);die;
                                      $sno=1;
                                      foreach($serviceproviders as $service_pro)
                                      { 
                                        ?>
                            <option value="<?=$service_pro?>"
                              <?=$service_pro==$service_detail['Email']->email_services ? 'selected' : ''?>>
                              <?=$service_pro?>
                            </option>
                            <?php  }
                                    ?>
                          </select>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3 col-xl-3">
                        <div class="form-group">
                          <label for="adminmail"><strong>Admin Mail Id</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="adminmail" id="adminmail" class="form-control"
                            placeholder="Enter Admin Mail Id"
                            value="<?=$service_detail['Email']->email_admin_mail_id?>">
                        </div>
                      </div>


                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="booking_date"><strong>Booking Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="email_booking_date" name="email_booking_date"
                            value="<?=$service_detail['Email']->email_booking_date?>">
                        </div>
                      </div>

                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="brand_name"><strong>Renewal Years</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" onkeyup="emailprice()" autocomplete="off"
                            id="email_year" name="email_year" placeholder="Enter Year" onkeypress='validate(event)'
                            value="<?=$service_detail['Email']->email_year?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for=""><strong>Renewal Date</strong></label>
                          <?php 
                          $StaringDate = empty($service_detail['Email']->email_booking_date) ? strtotime(date("Y-m-d")) : date('Y-m-d',strtotime($service_detail['Email']->email_booking_date));
                          $newEndingDate =strtotime( "+ 1 year",$StaringDate);?>
                          <input type="date" class="form-control" id="email_rDate" name="email_rDate"
                            value="<?=date('Y-m-d',strtotime($service_detail['Email']->email_rDate))?>" readonly>
                        </div>
                      </div>

                      <div class="col-md-3 col-lg-3 col-xl-3">
                        <input type="radio" name="emailsizetype" id="emb" value="mb"><label for="emb">
                          &nbsp;MB</label>
                        <input type="radio" name="emailsizetype" id="egb" value="gb" checked><label for="egb">
                          &nbsp;GB</label>
                      </div>

                      <?php 
                     $email_datas =  json_decode($service_detail['Email']->email_data);
                     $emailCounter = 0;
                     foreach($email_datas as $datas){
                      $emailCounter = count($datas);
                      foreach($datas as $data_key=>$data){
                        $data_key = $data_key+1;
                      ?>
                      <div id="rows" class="row col-md-12" style="margin-bottom:10px">
                        <div class="col-md-4 col-lg-4 col-xl-4 row">
                          <div class="form-group row">

                            <div class="col-md-12">

                            </div>
                            <div class="col-md-12 col-lg-12 col-xl-12">
                              <input type="text" class="form-control" autocomplete="off" id="box_size<?php echo $sno;?>"
                                name="box_size[]" placeholder="Email Box size"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$data[0]?>">
                            </div>
                          </div>
                        </div>

                        <div class="col-md-3 col-lg-3 col-xl-3">
                          <div class="form-group">

                            <input type="text" class="form-control" id="numEmail<?=$data_key?>" name="numEmail[]"
                              onkeyup="emailprice(<?=$data_key?>)" placeholder="Number of Email"
                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                              value="<?=$data[1]?>">
                          </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xl-4">
                          <div class="form-group">

                            <input type="text" name="costpemail[]" id="costpemail<?=$data_key?>"
                              onkeyup="emailprice(<?=$data_key?>)" class="form-control"
                              placeholder="Enter Cost Per Email"
                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                              value="<?=$data[2]?>">
                          </div>
                        </div>

                      </div>
                      <?php } } ?>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="cost"><strong>Total Cost</strong></label>
                          <input type="text" name="tcostemail" id="tcostemail" class="form-control"
                            placeholder="Total Price" readonly value="<?=$service_detail['Email']->email_total_price?>">
                        </div>
                      </div>

                    </div>
                  </section>
                  <section id="hostingform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Hosting</h2>
                      <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group row">
                          <div class="col-md-4 col-lg-4 col-xl-4" id="host-Domain">
                            <div class="form-group">
                              <label for="hostDomain"><strong>Domain Name</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="text" class="form-control" name="hostDomain" id="hostDomain"
                                placeholder="Enter Domain Name"
                                value="<?=$service_detail['Hosting']->hosting_domain_name?>">
                            </div>
                          </div>
                          <div class="col-md-8 col-lg-8 col-xl-8 row form-group">
                            <div class="col-md-12 col-lg-12 col-xl-12">

                              <label for="hostingspace"><strong>Hosting Space</strong>
                                <font color="#FF0000">*</font>
                              </label>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <input type="text" class="form-control" id="hostingspace" name="hostingspace"
                                placeholder="Enter Hosting Space" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['Hosting']->hosting_space?>">
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <?php $hosting_types = array('mb'=>'MB','gb'=>'GB');
                                foreach($hosting_types as $hosting_types_key=>$hosting_type){
                              ?>
                              <input type="radio" name="hspacetype" id="<?=$hosting_types_key?>"
                                <?=$hosting_types_key==$service_detail['Hosting']->hosting_space_type ? 'checked' : ''?>
                                value="<?=$hosting_types_key?>"><label for="<?=$hosting_types_key?>">
                                &nbsp;<?=$hosting_type?></label>
                              <?php } ?>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="hpanel"><strong>Panel</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <!-- <input type="text" class="form-control" id="hpanel" name="hpanel" placeholder="Enter Panel" autocomplete="off" > -->
                              <select name="hpanel" id="hpanel" class="form-control" onchange="otherform()">
                                <option disabled selected>Select</option>
                                <?php
                                            $panels = array('Rediff','GoDaddy','Hostgator','Dedicated');
                                            $sno=1;
                                            foreach($panels as $panel)
                                            { 
                                              ?>
                                <option value="<?=$panel?>"
                                  <?=$service_detail['Hosting']->hosting_panel==$panel ? 'selected' : ''?>><?=$panel?>
                                </option>
                                <?php  }
                                            ?>
                                <option value="other" id="other"
                                  <?=$service_detail['Hosting']->hosting_panel=='other' ? 'selected' : ''?>>Other
                                </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4" id="otherform">
                            <div class="form-group">
                              <label for="other"><strong>Other Panel</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="text" class="form-control" name="otherpanel"
                                value="<?=$service_detail['Hosting']->hosting_panel=='other' ? $service_detail['Hosting']->hosting_panel_name : ''?>"
                                placeholder="Enter panel name">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="host_booking_date"><strong>Booking Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="host_booking_date" name="host_booking_date"
                                value="<?php echo date("Y-m-d");?>"
                                value="<?=$service_detail['Hosting']->hosting_booking_date?>">
                            </div>
                          </div>

                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="hostingyear"><strong>Renewal Year</strong></label>
                              <input type="text" class="form-control" id="hostingyear" name="hostingyear"
                                value="<?=$service_detail['Hosting']->hosting_renew_year?>" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="host_ren_date"><strong>Renewal Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="host_ren_date" name="host_ren_date"
                                value="<?=$service_detail['Hosting']->hosting_renew_date?>" readonly>
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="hostingcost"><strong>Cost/Year</strong></label>
                              <input type="text" class="form-control" id="hostingcost" name="hostingcost"
                                placeholder="Enter Hosting Cost" autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['Hosting']->hosting_price?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="thostingcost"><strong>Total Cost</strong></label>
                              <input type="text" class="form-control" id="thostingcost" name="thostingcost"
                                placeholder="Total Hosting Cost" readonly
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['Hosting']->hosting_total_price?>">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="sslform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SSL</h2>
                      <div class="col-md-12 col-lg-12 col-xl-12">
                        <div class="form-group row">
                          <div class="col-md-4 col-lg-4 col-xl-4" id="ssl-Domain">
                            <div class="form-group">
                              <label for="sslDomain"><strong>Domain Name</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="text" class="form-control" name="sslDomain" id="sslDomain"
                                placeholder="Enter Domain Name" value="<?=$service_detail['SSL']->ssl_domain_name?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="ssl_booking_date"><strong>Booking Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="ssl_booking_date" name="ssl_booking_date"
                                value="<?php echo date("Y-m-d");?>"
                                value="<?=$service_detail['SSL']->ssl_booking_date?>">
                            </div>
                          </div>

                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="sslyear"><strong>Renewal Year</strong></label>
                              <input type="text" class="form-control" id="sslyear" name="sslyear" value="1"
                                autocomplete="off"
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['SSL']->ssl_renew_year?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="ssl_ren_date"><strong>Renewal Date</strong>
                                <font color="#FF0000">*</font>
                              </label>
                              <input type="date" class="form-control" id="ssl_ren_date" name="ssl_ren_date"
                                value="<?=date("Y-m-d",$newEndingDate)?>" readonly
                                value="<?=$service_detail['SSL']->ssl_renew_date?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="sslcost"><strong>Cost/Year</strong></label>
                              <input type="number" class="form-control" id="sslcost" name="sslcost"
                                placeholder="Enter SSL Cost" autocomplete="off"
                                value="<?=$service_detail['SSL']->ssl_price?>">
                            </div>
                          </div>
                          <div class="col-md-4 col-lg-4 col-xl-4">
                            <div class="form-group">
                              <label for="tsslcost"><strong>Total Cost</strong></label>
                              <input type="text" class="form-control" id="tsslcost" name="tsslcost"
                                placeholder="Total SSL Cost" readonly
                                oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                                value="<?=$service_detail['SSL']->ssl_total_price?>">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="websiteform">
                    <div class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Website Design</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="urlwebdesign"><strong>URL For Website Design</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="urlwebdesign" name="urlwebdesign"
                            placeholder="Enter Website URL" autocomplete="off"
                            value="<?=$service_detail['Web Designing']->web_desigin_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="webcost"><strong>Price</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="webcost" id="webcost" class="form-control"
                            placeholder="Enter Price For Webdesign"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Web Designing']->web_desigin_price?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="softwareform">
                    <div class="row">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Software Development</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="softdetail"><strong>Detail</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="softdetail" name="softdetail"
                            placeholder="Enter Software Detail" autocomplete="off"
                            value="<?=$service_detail['Software Development']->Software_development_detail?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="softcost"><strong>Price</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" name="softcost" id="softcost" class="form-control"
                            onkeyup="Softwareprice()" placeholder="Enter Price Of Software"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Software Development']->Software_development_price?>">
                        </div>
                      </div>
                      <div class="col-md-2 col-lg-2 col-xl-2" style="padding-top: 20px; height: 75px;">
                        <div class="form-group">
                          <input type="checkbox" id="maint"
                            <?=!empty($service_detail['Software Development']->Software_development_maintenence) ? 'checked' : ''?>
                            name="maintenence" value="maintenence"><label for="maint">&nbsp;Maintenence</label>
                        </div>
                      </div>
                      <div id="maintenenceform" class="col-md-8 col-lg-8 col-xl-8 row"
                        style="display:none;margin: 0;padding: 0;">
                        <div class="col-md-6 col-lg-6 col-xl-6">
                          <div class="form-group">
                            <label for="maintcost"><strong>Maintenence Price/Year</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <input type="text" name="maintcost" id="maintcost" class="form-control"
                              onkeyup="Softwareprice()" placeholder="Enter Maintenence Price"
                              oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                              onclick="get_maintenence_form(this.value)"
                              value="<?=$service_detail['Software Development']->Software_development_maintenence_price?>">
                          </div>
                        </div>
                        <div class="col-md-6 col-lg-6 col-xl-6">
                          <div class="form-group">
                            <label for="maintyear"><strong>Maintenence Time (Year)</strong>
                              <font color="#FF0000">*</font>
                            </label>
                            <input type="text" name="maintyear" id="maintyear" class="form-control"
                              onkeyup="Softwareprice()" placeholder="Enter Maintenence Time In Year"
                              value="<?=$service_detail['Software Development']->Software_development_maintenence_year?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="tpricesoft"><strong>Total Price </strong></label>
                          <input type="text" name="tpricesoft" id="tpricesoft" class="form-control"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Software Development']->Software_development_total_price?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="whatsappform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">Whatsapp</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="nWhatsapp"><strong>Quantity Of Whatsapp</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="nWhatsapp" name="nWhatsapp"
                            onkeyup="whatsappprice()" placeholder="Enter Whatsapp" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Whatsapp']->whatsapp_qty?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="pWhatsapp"><strong>Price/Whatsapp</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="pWhatsapp" name="pWhatsapp"
                            onkeyup="whatsappprice()" placeholder="Enter Whatsapp Price" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Whatsapp']->whatsapp_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="twhatsprice"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" id="twhatsprice" name="twhatsprice"
                            placeholder="Total Price" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['Whatsapp']->whatsapp_qty*$service_detail['Whatsapp']->whatsapp_price?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="smsform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 text-center h4 border-bottom">SMS</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smsqty"><strong>SMS Quantinty</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smsqty" name="smsqty" onkeyup="smsprice()"
                            placeholder="Enter SMS Quantinty" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMS']->sms_qty?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="pricesms"><strong>Price/SMS (Rupees)</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="pricesms" name="pricesms" onkeyup="smsprice()"
                            placeholder="Enter SMS Price (Rupees)" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMS']->sms_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smsTotalprice"><strong>Total Price</strong></label>
                          <input type="text" class="form-control" id="smsTotalprice" name="smsTotalprice"
                            placeholder="Total Price (Rupees)" readonly
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMS']->sms_qty*$service_detail['SMS']->sms_price?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="seoform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SEO</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seourl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="seourl" name="seourl"
                            placeholder="Enter URL For SEO" autocomplete="off"
                            value="<?=$service_detail['SEO']->seo_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seocost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="seocost" name="seocost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SEO']->seo_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="seodate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="seodate" name="seodate"
                            value="<?=date("Y-m-d");?>" value="<?=$service_detail['SEO']->seo_date?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="smoform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SMO</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smourl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smourl" name="smourl"
                            placeholder="Enter URL For SMO" autocomplete="off"
                            value="<?=$service_detail['SMO']->smo_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smocost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smocost" name="smocost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMO']->smo_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smodate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="smodate" value="<?=date("Y-m-d");?>"
                            name="smodate" value="<?=$service_detail['SMO']->smo_date?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="smmform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">SMM</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smmurl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smmurl" name="smmurl"
                            placeholder="Enter URL For SMM" autocomplete="off"
                            value="<?=$service_detail['SMM']->smm_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smmcost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="smmcost" name="smmcost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['SMM']->smm_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="smmdate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="smmdate" value="<?=date("Y-m-d");?>"
                            name="smmdate" value="<?=$service_detail['SMM']->smm_date?>">
                        </div>
                      </div>
                    </div>
                  </section>
                  <section id="ppcform">
                    <div class="row col-md-12 col-lg-12 col-xl-12">
                      <h2 class="col-md-12 col-lg-12 col-xl-12 text-center h4 border-bottom">PPC</h2>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppcurl"><strong>URL</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="ppcurl" name="ppcurl"
                            placeholder="Enter URL For PPC" autocomplete="off"
                            value="<?=$service_detail['PPC']->ppc_url?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppccost"><strong>Monthly Cost</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="text" class="form-control" id="ppccost" name="ppccost"
                            placeholder="Enter Monthly Cost" autocomplete="off"
                            oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"
                            value="<?=$service_detail['PPC']->ppc_price?>">
                        </div>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="form-group">
                          <label for="ppcdate"><strong>Start Date</strong>
                            <font color="#FF0000">*</font>
                          </label>
                          <input type="date" class="form-control" id="ppcdate" value="<?=date("Y-m-d");?>"
                            name="ppcdate" value="<?=$service_detail['PPC']->ppc_date?>">
                        </div>
                      </div>
                  </section>
                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<script>
function validate(evt) {
  var theEvent = evt || window.event;

  // Handle paste
  if (theEvent.type === 'paste') {
    key = event.clipboardData.getData('text/plain');
  } else {
    // Handle key press
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode(key);
  }
  var regex = /[0-9]|\./;
  if (!regex.test(key)) {
    theEvent.returnValue = false;
    if (theEvent.preventDefault) theEvent.preventDefault();
  }
}

var checked = false;

function checkedAll() {
  var aa = document.getElementsByName("services[]");
  checked = document.getElementById('select_all').checked;

  for (var i = 0; i < aa.length; i++) {
    aa[i].checked = checked;
  }
}
</script>
<script type="application/javascript">
function domainprice() {
  var price = document.getElementById('price').value;
  var year = document.getElementById('year').value;
  var totalprice = price + +year;
  document.getElementById('dtotalprice').value = totalprice;
}

function Softwareprice() {
  var price = document.getElementById('softcost').value;
  var maintyear = document.getElementById('maintyear').value;
  var maintcost = document.getElementById('maintcost').value;
  var maint_price = maintyear * maintcost;
  var total_price = maint_price + +price;
  document.getElementById('tpricesoft').value = total_price;
}

function whatsappprice() {
  var wqnty = document.getElementById('nWhatsapp').value;
  var price = document.getElementById('pWhatsapp').value;

  var tprice = wqnty * price;
  //alert(tprice);
  document.getElementById('twhatsprice').value = tprice;
}

function smsprice() {
  var smsqty = document.getElementById('smsqty').value;
  var pricesms = document.getElementById('pricesms').value;

  var tprice = smsqty * pricesms;
  //alert(tprice);
  document.getElementById('smsTotalprice').value = tprice;
}

$(function() {
  $('input[name="Q_date"]').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    minYear: 1901,
    maxYear: parseInt(moment().format('YYYY'), 10)
  });
});





$("#sale_total_order_value").keyup(function() {
  // alert('qqqqqq');
  var total_value = Number(this.value);
  var advance_payment = Number($('#sale_advance_payment').val());
  var gst_amount = Number($('#gst_amount').val());
  var pending_amount = (total_value + gst_amount) - advance_payment;
  $('#sale_pending_payment').val(pending_amount);
});
(function() {
  var url = window.location.pathname;;
  var id = url.substring(url.lastIndexOf('/') + 1);
  document.getElementById('sale_id').value = id;
})();
(function() {
  $('#booking_date').on('input', function() {
    var date = new Date($('#booking_date').val());
    var yearval = new $('#year').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('rDate').value = rendate;
    //alert(rendate);
  });

  $('#year').on('keyup', function() {
    var date = new Date($('#booking_date').val());
    var yearval = new $('#year').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');

    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('rDate').value = rendate;
    //alert(rendate);
  });

  $('#year').on('keyup', function() {
    var price = new Number($('#price').val());
    var yearval = new $('#year').val();
    var totalprice = price * yearval;
    document.getElementById('dtotalprice').value = totalprice;
  });
  $('#price').on('keyup', function() {
    var price = new Number($('#price').val());
    var yearval = new $('#year').val();
    var totalprice = price * yearval;
    document.getElementById('dtotalprice').value = totalprice;
  });
})();

(function() {
  $('#host_booking_date').on('input', function() {
    var date = new Date($('#host_booking_date').val());
    var yearval = new $('#hostingyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('host_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#host_booking_date').on('input', function() {
    var date = new Date($('#host_booking_date').val());
    var yearval = new $('#hostingyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('host_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#hostingyear').on('keyup', function() {
    var date = new Date($('#host_booking_date').val());
    var hyearval = new $('#hostingyear').val();
    if (hyearval == '') {
      hyear = date.getFullYear() + 1 + +hyearval;
    } else {
      hyear = date.getFullYear() + +hyearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');

    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [hyear, month, day].join('-');
    document.getElementById('host_ren_date').value = rendate;
    //alert(rendate);
  });

  $('#hostingyear').on('keyup', function() {
    var hostprice = new Number($('#hostingcost').val());
    var hyearval = new $('#hostingyear').val();
    var htotalprice = hostprice * hyearval;
    document.getElementById('thostingcost').value = htotalprice;
  });
  $('#hostingcost').on('keyup', function() {
    var hostprice = new Number($('#hostingcost').val());
    var hyearval = new $('#hostingyear').val();
    var htotalprice = hostprice * hyearval;
    document.getElementById('thostingcost').value = htotalprice;
  });
})();

(function() {
  $('#ssl_booking_date').on('input', function() {
    var date = new Date($('#ssl_booking_date').val());
    var yearval = new $('#sslyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('ssl_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#ssl_booking_date').on('input', function() {
    var date = new Date($('#ssl_booking_date').val());
    var yearval = new $('#sslyear').val();
    if (yearval == '') {
      year = date.getFullYear() + 1 + +yearval;
    } else {
      year = date.getFullYear() + +yearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');
    year = date.getFullYear() + +yearval;
    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [year, month, day].join('-');
    document.getElementById('ssl_ren_date').value = rendate;
    //alert(rendate);
  });
  $('#sslyear').on('keyup', function() {
    var date = new Date($('#ssl_booking_date').val());
    var syearval = new $('#sslyear').val();
    if (syearval == '') {
      syear = date.getFullYear() + 1 + +syearval;
    } else {
      syear = date.getFullYear() + +syearval;
    }
    day = date.getDate();
    month = String(date.getMonth() + 1).padStart(2, '0');

    //String(date.getMonth() + 1).padStart(2, '0');
    var rendate = [syear, month, day].join('-');
    document.getElementById('ssl_ren_date').value = rendate;
    //alert(rendate);
  });

  $('#sslyear').on('keyup', function() {
    var sslprice = new Number($('#sslcost').val());
    var syearval = new $('#sslyear').val();
    var stotalprice = sslprice * syearval;
    document.getElementById('tsslcost').value = stotalprice;
  });
  $('#sslcost').on('keyup', function() {
    var sslprice = new Number($('#sslcost').val());
    var syearval = new $('#sslyear').val();
    var stotalprice = sslprice * syearval;
    document.getElementById('tsslcost').value = stotalprice;
  });
})();
$("form#serviceEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("serviceID", '<?=$service->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {
          location.href = "<?= base_url('edit-service/'.base64_encode($service->id))?>";
        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>

<script>
$(document).ready(function() {
  $('#show-services').click(function(event) {
    event.stopPropagation();
    $("#service-list").slideToggle("fast");
  });
  $("#service-list").on("click", function(event) {
    event.stopPropagation();
  });
});

$(document).on("click", function() {
  $("#service-list").hide();
});
// $(document).on('click', '#show-services', function(){ 
//  $("#service-list").toggle(this.show);
// });
</script>
<script>
$("#show-services").on("click", function() {
  if ($('input#domain').is(':checked')) {
    $("#domainform").show();
  } else {
    $("#domainform").hide();
  }

  if ($('input#email').is(':checked')) {
    $("#emailform").show();
  } else {
    $("#emailform").hide();
  }

  if ($('input#hosting').is(':checked')) {
    $("#hostingform").show();
  } else {
    $("#hostingform").hide();
  }

  if ($('input#ssl').is(':checked')) {
    $("#sslform").show();
  } else {
    $("#sslform").hide();
  }

  if ($('input#webDesigning').is(':checked')) {
    $("#websiteform").show();
  } else {
    $("#websiteform").hide();
  }

  if ($('input#softwareDevelopment').is(':checked')) {
    $("#softwareform").show();
  } else {
    $("#softwareform").hide();
  }
  if ($('input#whatsapp').is(':checked')) {
    $("#whatsappform").show();
  } else {
    $("#whatsappform").hide();
  }
  if ($('input#sms').is(':checked')) {
    $("#smsform").show();
  } else {
    $("#smsform").hide();
  }
  if ($('input#seo').is(':checked')) {
    $("#seoform").show();
  } else {
    $("#seoform").hide();
  }

  if ($('input#smo').is(':checked')) {
    $("#smoform").show();
  } else {
    $("#smoform").hide();
  }
  if ($('input#sms').is(':checked')) {
    $("#smmform").show();
  } else {
    $("#smmform").hide();
  }
  if ($('input#ppc').is(':checked')) {
    $("#ppcform").show();
  } else {
    $("#ppcform").hide();
  }
});
// Form hide/show js
$(document).ready(function() {
  $('#domain').click(function() {
    $("#domainform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#domain').click(function() {
    if ($('input#domain').is(':checked')) {
      $("#email-domain").hide();
    } else {
      $("#email-domain").show();
    }
  });
  $('#email').click(function() {
    $("#emailform").toggle(this.checked);
    if ($('input#domain').is(':checked')) {
      $("#email-domain").hide();
    } else {
      $("#email-domain").show();
    }
  });

});

$(document).ready(function() {
  $('#domain').click(function() {
    if ($('input#domain').is(':checked')) {
      $("#host-Domain").hide();
    } else {
      $("#host-Domain").show();
    }
  });
  $('#hosting').click(function() {
    $("#hostingform").toggle(this.checked);
    if ($('input#domain').is(':checked')) {
      $("#host-Domain").hide();
    } else {
      $("#host-Domain").show();
    }
  });

});

$(document).ready(function() {
  $('#domain').click(function() {
    if ($('input#domain').is(':checked')) {
      $("#ssl-Domain").hide();
    } else {
      $("#ssl-Domain").show();
    }
  });
  $('#ssl').click(function() {
    $("#sslform").toggle(this.checked);
    if ($('input#domain').is(':checked')) {
      $("#ssl-Domain").hide();
    } else {
      $("#ssl-Domain").show();
    }
  });

});


/////////////get price per email////////////////////
function emailprice(val) {

  let counter = <?= $emailCounter;?>;
  var email_year = document.getElementById('email_year').value;
  let alltotal = 0;
  for (let i = 1; i <= counter; i++) {
    var totalcost = new Array();

    var numid = 'numEmail' + i;
    var costid = 'costpemail' + i;
    var emailnum = new Number($("#" + numid).val());
    var emailcost = new Number($("#" + costid).val());
    totalcost[i] = emailnum * emailcost;

    alltotal += totalcost[i];
  }
  document.getElementById('tcostemail').value = alltotal * email_year;
  // alert(alltotal);
}
</script>
<script>
$('#email_booking_date').on('input', function() {
  var date = new Date($('#email_booking_date').val());
  var yearval = new $('#email_year').val();
  if (yearval == '') {
    year = date.getFullYear() + 1 + +yearval;
  } else {
    year = date.getFullYear() + +yearval;
  }
  day = date.getDate();
  month = String(date.getMonth() + 1).padStart(2, '0');
  year = date.getFullYear() + +yearval;
  //String(date.getMonth() + 1).padStart(2, '0');
  var rendate = [year, month, day].join('-');
  document.getElementById('email_rDate').value = rendate;
  //alert(rendate);
});

$('#email_year').on('keyup', function() {
  var date = new Date($('#email_booking_date').val());
  var yearval = new $('#email_year').val();
  if (yearval == '') {
    year = date.getFullYear() + 1 + +yearval;
  } else {
    year = date.getFullYear() + +yearval;
  }
  day = date.getDate();
  month = String(date.getMonth() + 1).padStart(2, '0');

  //String(date.getMonth() + 1).padStart(2, '0');
  var rendate = [year, month, day].join('-');
  document.getElementById('email_rDate').value = rendate;
  //alert(rendate);
});
</script>

/////////////get price per email////////////////////
<script>
$(document).ready(function() {
  $('#hosting').click(function() {
    $("#hostingform").toggle(this.checked);
  });
});

$(document).ready(function() {
  $('#ssl').click(function() {
    $("#sslform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#softwareDevelopment').click(function() {
    $("#softwareform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#webDesigning').click(function() {
    $("#websiteform  ").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#whatsapp').click(function() {
    $("#whatsappform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#sms').click(function() {
    $("#smsform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#seo').click(function() {
    $("#seoform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#smo').click(function() {
    $("#smoform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#smm').click(function() {
    $("#smmform").toggle(this.checked);
  });
});
$(document).ready(function() {
  $('#ppc').click(function() {
    $("#ppcform").toggle(this.checked);
  });
});
</script>
<script>
$(document).ready(function() {
  $(function() {
    $("#maint").on("click", function() {
      $("#maintenenceform").toggle(this.checked);
    });

  });
});


function otherform() {
  if ($('#hpanel').val() == 'other') {
    $("#otherform").show();
  } else {
    $("#otherform").hide();
  }
}
</script>
<script>
function view_services(id) {
  // alert(id);
  $.ajax({
    url: '<?=base_url('Product_Form_Controller/get_product_detail/')?>' + id,
    type: 'post',
    dataType: 'html',
    success: function(response) {
      // alert(response);
      //console.log(response);
      $('#viewServices_detail').html(response);
      togglePay();
    }
  });
}
</script>