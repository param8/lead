<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>
          <?php if($this->session->userdata('user_type')!=1 && $permission[4]=='Upload'){?>
          <div class="col-sm-3 col-6">
            <div class="float-right">
              <a type="button" class="btn btn-warning btn-sm" href="javascript:void(0)" style="float: right"
                onclick="upload_customer()">Import <?=$page_title?></a>
            </div>
          </div>
          <?php } ?>
          <?php if($this->session->userdata('user_type')!=1 && $permission[1]=='Add'){?>
          <div class="col-sm-3 col-6">
            <div class="float-right">
              <a type="button" class="btn btn-primary btn-sm" href="<?=base_url('create-customer')?>"
                style="float: right">Create <?=$page_title?></a>
            </div>
          </div>
          <?php } ?>

        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="card">
            <div class="card-body">
              <div class="text-center text-danger">
                <?//=$message?>
              </div>
              <?php if($this->session->userdata('user_type')!=1 && $permission[8]=='Filter'){?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="card">
                    <div class="card-body">
                      <form method="post" id="filter-data" action="<?=base_url('Customer/setSessionCustomer');?>">
                        <div class="row">



                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>Months</label>
                              <?php $months = array('0'=>'All Months','01'=>'January','02'=>'February','03'=>'March','04'=>'April','05'=>'May','06'=>'June','07'=>'July','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');?>
                              <select id="monthname_customer" class="form-control" name="monthname_customer"
                                onchange="this.form.submit();">
                                <?php foreach($months as $month_key=>$month){?>
                                <option value="<?=$month_key?>"
                                  <?=(!empty($this->session->userdata('monthname_customer')) AND $this->session->userdata('monthname_customer')==$month_key) ? 'selected' :  '' ?>>
                                  <?=$month?></option>
                                <?php } ?>
                              </select>
                              <span class="close_button" onclick="resetMonthName()" id="resetMonthName"
                                <?=empty($this->session->userdata('monthname_customer')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>

                          <?php //if($this->session->userdata('user_type')==2){?>
                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>Sale Users</label>
                              <select id="assign_user_customer" class="form-control" name="assign_user_customer"
                                onchange="this.form.submit();">
                                <option value="">All Users</option>
                                <?php foreach($users as $user){?>
                                <option value="<?=$user->id?>"
                                  <?=$this->session->userdata('assign_user_customer')==$user->id ? 'selected':'' ;?>>
                                  <?=$user->name.'('.$user->roleName.')'?></option>
                                <?php } ?>
                              </select>
                              <span class="close_button" onclick="resetAssignUser()" id="resetAssignUser"
                                <?=empty($this->session->userdata('assign_user_customer')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>
                          <?php //} ?>

                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>From Date</label>
                              <div class="input-group">
                                <input type="text" class="form-control" onfocus="(this.type='date')"
                                  placeholder="From Date" name="customer_from_date" id="customer_from_date"
                                  value="<?=!empty($this->session->userdata('customer_from_date')) ? $this->session->userdata('customer_from_date'):'';?>"
                                  onchange="this.form.submit();">
                              </div>
                              <span class="close_button" onclick="resetFromDate()" id="resetFromDate"
                                <?=empty($this->session->userdata('customer_from_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>
                          <div class="col-md-2 col-lg-2 col-xl-2 col-sm-6">
                            <div class="reset_btn">
                              <label>To Date</label>
                              <div class="input-group">
                                <input type="text" class="form-control" onfocus="(this.type='date')"
                                  placeholder="To Date" name="customer_to_date" id="customer_to_date"
                                  value="<?=!empty($this->session->userdata('customer_to_date')) ? $this->session->userdata('customer_to_date') :'';?>"
                                  onchange="this.form.submit();">
                              </div>
                              <span class="close_button" onclick="resetToDate()" id="resetToDate"
                                <?=empty($this->session->userdata('customer_to_date')) ? 'style="display:none;"' : 'style="display:block;"'?>>&#10539;</span>
                            </div>
                          </div>


                        </div>
                      </form>
                      <hr>
                      <?php } ?>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="text-center text-danger">
                                <?//=$message?>
                              </div>
                              <?php if($this->session->userdata('user_type')==1){?>
                              <div class="row">
                                <div class="col-sm-6">
                                  <select class="form-control" name="customerAdminID" id="customerAdminID"
                                    onchange="getcustomerAdminSession(this.value)">
                                    <option value="">Select Admin</option>
                                    <?php foreach($admins as $admin){?>
                                    <option value="<?=$admin->id?>"
                                      <?=$admin->id == $this->session->userdata('customerAdminID') ? 'selected' : ''?>>
                                      <?=$admin->name?>
                                    </option>
                                    <?php } ?>
                                  </select>

                                </div>

                              </div>
                              <hr>
                              <?php } ?>

                              <div class="text-center ">
                                <b><?=$this->session->userdata('user_type')!=1 ? $message : ''?></b>
                              </div>
                              <div class="table-responsive">
                                <table class=" table table-hover table-center mb-0" id="customerDataTable">
                                  <thead>
                                    <tr>
                                      <th>S.no.</th>
                                      <th>Action</th>
                                      <th>Name</th>
                                      <th>Phone</th>
                                      <th>Email</th>
                                      <th>Created Date</th>
                                      <th>Address</th>
                                      <th>City</th>
                                      <th>State</th>
                                      <th>Pincode</th>
                                    </tr>
                                  </thead>
                                  <tbody>

                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal fade" id="viewCustomer" tabindex="-1" role="dialog"
                  aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-eye text-warning"></i> View
                          <?=$page_title?>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                          onclick="close_modal()">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>

                      <div class="modal-body" id="view_data">

                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger " onclick="close_modal()" data-dismiss="modal"
                          aria-label="Close">Close</button>
                      </div>

                    </div>
                  </div>
                </div>

                <div class="modal fade" id="importCustomer" tabindex="-1" role="dialog"
                  aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-upload text-warning"></i> Import
                          <?=$page_title?>
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" onclick="close_modal()"
                          aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>

                      <form action="<?=base_url('Customer/import_customer_manual')?>" method="POST"
                        id="importCustomerForm" enctype="multipart/form-data">
                        <div class="modal-body">
                          <div class="col-md-12 col-lg-12 col-xl-12">
                            <div class="form-group">
                              <div id="duplicat_customer_show" style="display:none;color:red"></div>
                            </div>

                          </div>
                          <div class="row" id="form_data">

                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <div class="form-group">
                                <label>Users <span class="text-danger">*</span></label>
                                <select class="form-control " name="userID" id="userID">
                                  <option value="">Select User</option>
                                  <?php foreach($users as $user){?>
                                  <option value="<?=$user->id?>"><?=$user->name .'('.$user->roleName.')'?></option>
                                  <?php } ?>
                                </select>
                              </div>
                            </div>
                            <div class="col-md-6 col-lg-6 col-xl-6">
                              <div class="form-group">
                                <label>File <span class="text-danger">*</span></label>
                                <input type="file" class="form-control" name="file" id="file"
                                  accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                              </div>
                            </div>
                          </div>

                        </div>
                        <div class="modal-footer d-flex  " style="justify-content: space-between;">

                          <div>
                            <a href="<?=base_url('Customer/download_sample')?>" class="btn  btn-outline-warning ">Sample
                              <i class="fa fa-download"></i></a>
                          </div>
                          <div>
                            <button type="button" class="btn  btn-outline-danger " onclick="close_modal()"
                              data-dismiss="modal" aria-label="Close">Close <i class="fa fa-close"></i></button>
                            <button type="submit" class="btn  btn-outline-primary ">Upload <i
                                class="fa fa-upload"></i></button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>


                <script>
                $(document).ready(function() {
                  //alert('dfgfgf');
                  // $('#example').DataTable();
                  // } );
                  var dataTable = $('#customerDataTable').DataTable({
                    "processing": true,
                    "serverSide": true,
                    buttons: [{
                      extend: 'excelHtml5',
                      text: 'Download Excel'
                    }],
                    "order": [],
                    "ajax": {
                      url: "<?=base_url('Customer/ajaxCustomer/'.$uri)?>",
                      type: "POST"
                    },
                    "columnDefs": [{
                      "targets": [0],
                      "orderable": true,
                    }, ],
                  });
                });

                $("form#importCustomerForm").submit(function(e) {
                  $(':input[type="submit"]').prop('disabled', true);
                  e.preventDefault();
                  var formData = new FormData(this);
                  $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'json',
                    success: function(data) {
                      if (data.status == 200) {
                        toastNotif({
                          text: data.message,
                          color: '#5bc83f',
                          timeout: 10000,
                          icon: 'valid'
                        });
                        $(':input[type="submit"]').prop('disabled', false);
                        if (data.duplicat_customer == '') {
                          $('#duplicat_customer_show').hide();
                          ajaxSendMail(data.id);
                          setTimeout(function() {
                            location.href = "<?=base_url('customers')?>";
                          }, 2000)
                        } else {

                          var html = '';
                          $('#duplicat_customer_show').show();
                          var result = $('#duplicat_customer_show');
                          var array = data.duplicat_customer;
                          if (array) {
                            $.each(array, function(i) {
                              $.each(array[i], function(key, value) {
                                html += i + 1 + '.' + key + ' --- ' + value;
                              });
                              html += '<br>';
                            });
                            result.html(html);
                          }
                          $('#form_data').load(location.href + " #form_data");
                          ajaxSendMail(data.id)
                        }


                      } else if (data.status == 403) {
                        toastNotif({
                          text: data.message,
                          color: '#da4848',
                          timeout: 5000,
                          icon: 'error'
                        });

                        $(':input[type="submit"]').prop('disabled', false);
                      } else {
                        toastNotif({
                          text: data.message,
                          color: '#da4848',
                          timeout: 5000,
                          icon: 'error'
                        });
                        $(':input[type="submit"]').prop('disabled', false);
                      }
                    },
                    error: function() {}
                  });
                });


                function ajaxSendMail(id) {
                  $.ajax({
                    url: "<?=base_url('Send_mail/customerSendMail')?>",
                    type: 'POST',
                    data: {
                      id
                    },
                    success: function(data) {
                      toastNotif({
                        text: 'Mail sent successfully',
                        color: '#5bc83f',
                        timeout: 10000,
                        icon: 'valid'
                      });
                      setTimeout(function() {
                        location.reload();
                      }, 1000)

                    },

                  });
                }

                function delete_customer(id) {

                  Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                  }).then((result) => {
                    if (result.value) {
                      $.ajax({
                        url: '<?=base_url('Customer/delete')?>',
                        type: 'POST',
                        data: {
                          id
                        },
                        dataType: 'json',
                        success: function(data) {
                          if (data.status == 200) {
                            toastNotif({
                              text: data.message,
                              color: '#5bc83f',
                              timeout: 5000,
                              icon: 'valid'
                            });
                            setTimeout(function() {

                              location.href = "<?=base_url('customers')?>"

                            }, 1000)


                          } else if (data.status == 302) {
                            toastNotif({
                              text: data.message,
                              color: '#da4848',
                              timeout: 5000,
                              icon: 'error'
                            });

                          }
                        }
                      })
                    }
                  })
                }

                function getcustomerAdminSession(id) {
                  $.ajax({
                    url: "<?=base_url('Ajax_controller/getcustomerAdminSession')?>",
                    type: 'POST',
                    data: {
                      id
                    },
                    success: function(data) {
                      location.reload();
                    },

                  });
                }

                function view_customer(id) {
                  $.ajax({
                    url: "<?=base_url('Customer/customerViewForm')?>",
                    type: 'POST',
                    data: {
                      id
                    },
                    success: function(data) {
                      $('#viewCustomer').modal('show');
                      $('#view_data').html(data);
                    },

                  });
                }

                function upload_customer() {
                  $('#importCustomer').modal('show');
                }

                function close_modal() {
                  location.reload();
                  // $('#importCustomer').modal('hide');
                  // $('#viewCustomer').modal('hide');
                }


                function resetFromDate() {
                  $.ajax({
                    url: '<?=base_url("Customer/resetFromDateCustomer")?>',
                    type: 'POST',
                    data: {
                      ResetSesession: 'ResetSesession'
                    },
                    success: function(data) {
                      location.reload();
                    },
                  });
                }

                function resetToDate() {
                  $.ajax({
                    url: '<?=base_url("Customer/resetToDateCustomer")?>',
                    type: 'POST',
                    data: {
                      ResetSesession: 'ResetSesession'
                    },
                    success: function(data) {
                      location.reload();
                    },
                  });
                }

                function resetMonthName() {
                  $.ajax({
                    url: '<?=base_url("Customer/resetMonthNameCustomer")?>',
                    type: 'POST',
                    data: {
                      ResetSesession: 'ResetSesession'
                    },
                    success: function(data) {
                      location.reload();
                    },
                  });
                }

                function resetAssignUser() {
                  $.ajax({
                    url: '<?=base_url("Customer/resetAssignUserCustomer")?>',
                    type: 'POST',
                    data: {
                      ResetSesession: 'ResetSesession'
                    },
                    success: function(data) {
                      location.reload();
                    },
                  });
                }
                </script>