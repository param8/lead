<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">Change Password</h5>
      </div>

      <div class="card-body pl-10 pr-10">
        <form id="updateProfileForm" method="post" action="<?=base_url('User/update_password');?>"
          enctype="multipart/form-data">
          <div class="settings-form row">
            <div class="col-md-12">
              <div class="form-group">
                <label>Old Password <span class="star-red">*</span></label>
                <input type="password" name="old_password" class="form-control" placeholder="Enter Old Password">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>New Password <span class="star-red">*</span></label>
                <input type="password" name="new_password" id="new_password" class="form-control"
                  placeholder="Enter New Password">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Confirm Password <span class="star-red">*</span></label>
                <input type="password" class="form-control" name="confirm_password" id="confirm_password"
                  placeholder="Enter Confirm Password">
              </div>
            </div>

            <div class="form-group mb-0">
              <div class="settings-btns">
                <button type="submit" class="btn btn-orange">Change</button>

              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</div>

<script>
$("form#updateProfileForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$user->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.reload();

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$(document).ready(function() {
  get_city(<?=$user->state?>, <?=$user->id?>);
});
</script>