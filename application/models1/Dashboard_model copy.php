<?php 

class Dashboard_model extends CI_Model

{

	public function __construct()

	{
		parent::__construct();
	}

  public function get_sale_report($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    //print_r($usersID);die;
    $this->db->select('count(sales.id) as totalSales,SUM(sales.sale_amount) as totalSalesAmount,SUM(sales.advance_amount) as totalPayments, MONTHNAME(sales.sale_date) as month_name');
    $this->db->from('sales');
    $this->db->where($condition);
    $this->db->where('DATE_FORMAT(sales.sale_date,"%Y-%m")',date('Y-m'));
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
    return $this->db->get()->row();
   // echo $this->db->last_query();die;
  }

  public function get_sale_reports($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('users.*,SUM(sales.sale_amount) as totalSales,SUM(sales.advance_amount) as totalPayments, MONTHNAME(sales.sale_date) as month_name');
    $this->db->from('sales');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->where($condition);
    $this->db->where('DATE_FORMAT(sales.sale_date,"%Y-%m")',date('Y-m'));
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
    return $this->db->get()->result();
    //echo $this->db->last_query();die;
  }


  public function get_sale_reports_user_wise($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('users.id as user_id , users.name as user_name,sales.sale_amount as totalSales,sales.advance_amount as totalPayments, MONTHNAME(sales.sale_date) as month_name');
    $this->db->from('sales');
    $this->db->join('users','users.id = sales.sale_closed_by','left');
    $this->db->where($condition);
    $this->db->where('DATE_FORMAT(sales.sale_date,"%Y-%m")',date('Y-m'));
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
   return  $this->db->get()->result();
    // echo $this->db->last_query();die;
  }

  public function get_yearly_sale_report(){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $month = date('m');
		if($month >= 4)
		{
			$y = date('Y');
			$pt = date('Y', strtotime('+1 year'));
			$sfy = $y."-04-01";
			$efy = $pt."-03-31";
		}
		else
		{
			$y = date('Y', strtotime('-1 year'));
			$pt = date('Y');
			$sfy=$y."-04-01";
			$efy =$pt."-03-31";
		}

    $this->db->select('COUNT(id) as totalSales,SUM(sale_amount) as totalPayments, MONTHNAME(`sale_date`) as month_name');
    $this->db->from('sales');
    $this->db->where($condition);
    $this->db->where('DATE_FORMAT(sales.sale_date,"%Y-%m")',date('Y-m'));
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }

  }

  public function get_pending_amount_recieve($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));

    $this->db->select('Sum(amount) as total_pending_amount ,MONTHNAME(`created_at`) as month_name');
    $this->db->from('payment_history');
    $this->db->where($condition);
    $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m")',date('Y-m'));
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('payment_history.userID',$usersID );
      }
    }
    
    return $this->db->get()->row();
  }

  public function get_pending_amount($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('SUM(sales.pending_amount) as totalPending');
    $this->db->from('sales');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('sales.sale_closed_by',$usersID );
      }
    }
    return $this->db->get()->row();
  }

  public function get_financial_report_monthly($type){
    // 		$year = date('Y');
    // 		$month = date('Y-m');
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
        $cmonth = date('m');
        if($cmonth >= 4)
        {
        $month = date('Y-m');
        //$year = date('Y', strtotime('+1 year'));
        $year = date('Y');
        }
        else
        {
         $month = date('Y-m');
       // $year = date('Y', strtotime('-1 year'));
         $year = date('Y');
        }
        $max_year = $year+1;
        if($type == 'monthly_sale'){

            $this->db->select('Sum(sale_amount) as saleamount,sale_date');
            $this->db->from('sales');
            $this->db->where('sale_date BETWEEN "'. $year.'-04-01'. '" and "'. $max_year.'-03-31'.'"');
            if($this->session->userdata('user_type')!=2){
              if(!empty($usersID)){
                $this->db->where_in('sales.sale_closed_by',$usersID );
              }
            }
            $this->db->group_by('DATE_FORMAT(sales.sale_date,"%Y-%m")');
            $query=$this->db->get()->result_array();
            //echo $this->db->last_query();die;
         } 
         if($type == 'recievedAmount'){

          $this->db->select('Sum(amount) as RecieveAmount,`created_at`');
          $this->db->from('payment_history');
          //$this->db->where($condition);
          $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") BETWEEN "'. $year.'-04-01'. '" and "'. $max_year.'-03-31'.'"');
          //$this->db->group_by('DATE_FORMAT(payment_history.created_at,"%Y-%m")');
          if($this->session->userdata('user_type')!=2){
            if(!empty($usersID)){
              $this->db->where_in('payment_history.userID',$usersID );
            }
          }
          $query=$this->db->get()->result_array();
           
         }
         if($type == 'pendingAmount'){

          $this->db->select('Sum(pending_amount) as total_pending_payment,sale_date');
          $this->db->from('sales');
          $this->db->where('sale_date BETWEEN "'. $year.'-04-01'. '" and "'. $max_year.'-03-31'.'"');
          if($this->session->userdata('user_type')!=2){
            if(!empty($usersID)){
              $this->db->where_in('sales.sale_closed_by',$usersID );
            }
          }
          $this->db->group_by('DATE_FORMAT(sales.sale_date,"%Y-%m")');
         
          $query=$this->db->get()->result_array();
            
         }
        
         return $query;
        
      }


      public function get_financial_report_year($type){
        // 		$year = date('Y');
        // 		$month = date('Y-m');
        $role = role();
        $usersID = user_role_permission($this->session->userdata('user_type'));
            $cmonth = date('m');
            if($cmonth >= 4)
            {
            $month = date('Y-m');
            //$year = date('Y', strtotime('+1 year'));
            $year = date('Y');
            }
            else
            {
             $month = date('Y-m');
           // $year = date('Y', strtotime('-1 year'));
             $year = date('Y');
            }
            $max_year = $year+1;
            if($type == 'monthly_sale'){
    
                $this->db->select('Sum(sale_amount) as saleamount,sale_date');
                $this->db->from('sales');
                $this->db->where('sale_date BETWEEN "'. $year.'-04-01'. '" and "'. $max_year.'-03-31'.'"');
                if($this->session->userdata('user_type')!=2){
                  if(!empty($usersID)){
                    $this->db->where_in('sales.sale_closed_by',$usersID );
                  }
                }
                $this->db->group_by('DATE_FORMAT(sales.sale_date,"%Y-%m")');
                $query=$this->db->get()->result_array();
                //echo $this->db->last_query();die;
             } 
             if($type == 'recievedAmount'){
    
              $this->db->select('Sum(amount) as RecieveAmount,`created_at`');
              $this->db->from('payment_history');
              //$this->db->where($condition);
              $this->db->where('DATE_FORMAT(payment_history.created_at,"%Y-%m-%d") BETWEEN "'. $year.'-04-01'. '" and "'. $max_year.'-03-31'.'"');
              //$this->db->group_by('DATE_FORMAT(payment_history.created_at,"%Y-%m")');
              if($this->session->userdata('user_type')!=2){
                if(!empty($usersID)){
                  $this->db->where_in('payment_history.userID',$usersID );
                }
              }
              $query=$this->db->get()->result_array();
               
             }
             if($type == 'pendingAmount'){
    
              $this->db->select('Sum(pending_amount) as total_pending_payment,sale_date');
              $this->db->from('sales');
              $this->db->where('sale_date BETWEEN "'. $year.'-04-01'. '" and "'. $max_year.'-03-31'.'"');
              if($this->session->userdata('user_type')!=2){
                if(!empty($usersID)){
                  $this->db->where_in('sales.sale_closed_by',$usersID );
                }
              }
              $this->db->group_by('DATE_FORMAT(sales.sale_date,"%Y-%m")');
             
              $query=$this->db->get()->result_array();
                
             }
            
             return $query;
            
          }

      public function team_monthly_target($monthname){
        $role = role();
        $usersID = user_role_permission($this->session->userdata('user_type'));
        $adminID = $this->session->userdata('adminID');
        $m_y = explode('-',$monthname);
        $current_month = $m_y[0]; 
        $current_year = $m_y[1]; 

        $this->db->select('users.*,role.id as roleID,role.name as roleName');
        $this->db->from('users');
        $this->db->join('role','role.id=users.user_type','left');
        $this->db->where('role.adminID',$adminID);
        if($this->session->userdata('user_type')!=2){
          if(!empty($usersID)){
            $this->db->where_in('users.id',$usersID );
          }
        }
        $active_users= $this->db->get()->result_array();

        $total_team_month_target=0;
        foreach($active_users as $user){
          $user_id = $user['id'];
          $user_month_target = $this->get_user_mon_target($current_month, $current_year, $user_id,$adminID);
          $total_team_month_target += $user_month_target;
        }
    
        return $total_team_month_target;
      }
    
      function get_user_mon_target($month, $year, $user_id,$adminID){
        // return true;
        $role = role();
        $usersID = user_role_permission($this->session->userdata('user_type'));
        $search_date = $year.'-'.$month.'-01';	
        // select history list from target table
        $this->db->select('monthly_target_history.*');
        $this->db->from('monthly_target_history');
        $this->db->where('userID',$user_id);
        $this->db->where('adminID',$adminID);
   
        $this->db->order_by('created_at','desc');
        $target_history_result = $this->db->get()->result_array();
   

        // $target_history = "SELECT * FROM `monthly_target_history` WHERE userID=".$user_id." ORDER by created_at desc, target_date desc ";
        // $target_history_query = $this->db->query($target_history);
        // $target_history_result = $target_history_query->result_array();
    
        $target_array = array();
        foreach($target_history_result as $target_row)
        {
          if(!array_key_exists($target_row['target_date'],$target_array))
          {
            $target_array[$target_row['target_date']] = $target_row['target']; 	
          }
        }
    
        $user_monthly_target=0;	
        foreach($target_array as $key => $value)
        {
          if($search_date >= $key)
          {
            $user_monthly_target = $value;
            break;
          }
        }
    
        return $user_monthly_target;	
      }


  
}