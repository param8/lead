<?php 

class Followup_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

  
	function make_query($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup.id as followupID,followup.remark as followupRemark,followup.lead_status as followupLeadStatus,followup.followup_date as FollowupDate,followup.followup_time as FollowupTime,followup.status as followupStatus,followup.created_at as followupCreatedAt,followup.updated_at as followupUpdated,followup.today_followup_status as today_followup_status');
    $this->db->from('followup');
    $this->db->join('enquiry','enquiry.id = followup.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup.lead_status','left');
    $this->db->join('users','users.id = followup.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('followup.userID',$usersID );
      }
    }
    if($this->session->userdata('customers_filter')){
      $this->db->where('customers.id',$this->session->userdata('customers_filter'));
    }

    if(!empty($this->session->userdata('followup_from_date')) AND !empty($this->session->userdata('followup_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('followup_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('followup_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_followup'))){ 
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%m")', $this->session->userdata('monthname_followup'));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_followup'))){
    $this->db->where('users.id', $this->session->userdata('assign_user_followup'));
   }

   if(!empty($this->session->userdata('lead_status_followup'))){
    $this->db->where('followup.lead_status', $this->session->userdata('lead_status_followup'));
   }

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('customers.company_name', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('followup.updated_at','desc');
    
  }
    function make_datatables($condition){
	  $this->make_query($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  function get_all_data($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup.id as followupID,followup.remark as followupRemark,followup.lead_status as followupLeadStatus,followup.followup_date as FollowupDate,followup.followup_time as FollowupTime,followup.status as followupStatus,followup.created_at as followupCreatedAt,followup.updated_at as followupUpdated,followup.today_followup_status as today_followup_status');
    $this->db->from('followup');
    $this->db->join('enquiry','enquiry.id = followup.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('followup.userID',$usersID );
      }
    }
    if($this->session->userdata('customers_filter')){
      $this->db->where('customers.id',$this->session->userdata('customers_filter'));
    }

    if(!empty($this->session->userdata('followup_from_date')) AND !empty($this->session->userdata('followup_to_date')) ){
      $this->db->group_start();
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") <=', date('Y-m-d',strtotime($this->session->userdata('followup_to_date'))));
        $this->db->group_end();
    }elseif(!empty($this->session->userdata('followup_from_date'))){
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y-%m-%d") >=', date('Y-m-d',strtotime($this->session->userdata('followup_from_date'))));
        
    }
    if(!empty($this->session->userdata('monthname_followup'))){ 
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%m")', $this->session->userdata('monthname_followup'));
        $this->db->where('DATE_FORMAT(enquiry.assigin_date,"%Y")', date('Y'));
    }

  if(!empty($this->session->userdata('assign_user_followup'))){
    $this->db->where('users.id', $this->session->userdata('assign_user_followup'));
   }

   if(!empty($this->session->userdata('lead_status_followup'))){
    $this->db->where('followup.lead_status', $this->session->userdata('lead_status_followup'));
   }


   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('customers.company_name', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('followup.updated_at','desc');
	   return $this->db->count_all_results();
  }


  public function get_followup($condition){
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.id as assigin_to_id,users.name as assigin_to_name,users.email as assigin_to_email,users.phone as assigin_to_phone,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup.id as followupID,followup.remark as followupRemark,followup.lead_status as followupLeadStatus,followup.followup_date as FollowupDate,followup.followup_time as FollowupTime,followup.status as followupStatus,followup.created_at as followupCreatedAt,followup.updated_at as followupUpdated,followup.today_followup_status as today_followup_status');
    $this->db->from('followup');
    $this->db->join('enquiry','enquiry.id = followup.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
	 return $this->db->get()->row();
  }
  public function get_followups($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.id as assigin_to_id,users.name as assigin_to_name,users.email as assigin_to_email,users.phone as assigin_to_phone,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup.id as followupID,followup.remark as followupRemark,followup.lead_status as followupLeadStatus,followup.followup_date as FollowupDate,followup.followup_time as FollowupTime,followup.status as followupStatus,followup.created_at as followupCreatedAt,followup.updated_at as followupUpdated,followup.today_followup_status as today_followup_status');
    $this->db->from('followup');
    $this->db->join('enquiry','enquiry.id = followup.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup.lead_status','left');
    $this->db->join('users','users.id = enquiry.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('followup.userID',$usersID );
      }
    }
	  return $this->db->get()->result();
    //echo $this->db->last_query();die;
  }

  public function store_followup($data){
	 $this->db->insert('followup',$data);
   return $this->db->insert_id();
  }


  public function update_followup($data,$condition){
	$this->db->where($condition);
	return $this->db->update('followup',$data);
  }

  public function store_followup_history($data){
   return $this->db->insert('followup_history',$data);
     
  }

  public function delete_followup($condition){
    $this->db->where($condition);
   return $this->db->delete('followup');
  }

  public function delete_followup_history($condition){
    $this->db->where($condition);
   return $this->db->delete('followup_history');
  }


   function make_query_history($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup_history.id as followupHistoryID,followup_history.remark as followupHistoryRemark,followup_history.lead_status as followupHistoryLeadStatus,followup_history.followup_date as FollowupHistoryDate,followup_history.followup_time as FollowupHistoryTime,followup_history.status as followupHistoryStatus,followup_history.created_at as followupHistoryCreatedAt,followup_history.updated_at as followHistoryupUpdated');
    $this->db->from('followup_history');
    $this->db->join('enquiry','enquiry.id = followup_history.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup_history.lead_status','left');
    $this->db->join('users','users.id = followup_history.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('followup_history.userID',$usersID );
      }
    }
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned_by.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('followup_history.id','desc');
    
  }
    function make_datatables_history($condition){
	  $this->make_query_history($condition);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_history($condition){
	  $this->make_query_history($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_history($condition)
  {
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup_history.id as followupHistoryID,followup_history.remark as followupHistoryRemark,followup_history.lead_status as followupHistoryLeadStatus,followup_history.followup_date as FollowupHistoryDate,followup_history.followup_time as FollowupHistoryTime,followup_history.status as followupHistoryStatus,followup_history.created_at as followupHistoryCreatedAt,followup_history.updated_at as followHistoryupUpdated');
    $this->db->from('followup_history');
    $this->db->join('enquiry','enquiry.id = followup_history.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup_history.lead_status','left');
    $this->db->join('users','users.id = followup_history.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('followup_history.userID',$usersID );
      }
    }
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('customers.name', $_POST["search"]["value"]);
    $this->db->or_like('customers.email', $_POST["search"]["value"]);
    $this->db->or_like('customers.phone', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_type', $_POST["search"]["value"]);
    $this->db->or_like('enquiry.lead_sourse', $_POST["search"]["value"]);
    $this->db->or_like('lead_status.name', $_POST["search"]["value"]);
    $this->db->or_like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('assigned_by.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('followup_history.id','desc');
	   return $this->db->count_all_results();
  }


  public function get_followups_history($condition){
    $role = role();
    $usersID = user_role_permission($this->session->userdata('user_type'));
    $this->db->select('enquiry.*,customers.name as customerName,customers.company_name,customers.phone as customerPhone,customers.alternate_no as customerAlternateNo,customers.email as customerEmail,customers.address as customerAddress,customers.state as customerState,customers.city as customerCity,customers.pincode as customerPincode,users.name as assigin_to_name,assigned.name as assigin_by_name,lead_status.name as leadStatus,followup_history.id as followupHistoryID,followup_history.remark as followupHistoryRemark,followup_history.lead_status as followupHistoryLeadStatus,followup_history.followup_date as FollowupHistoryDate,followup_history.followup_time as FollowupHistoryTime,followup_history.status as followupHistoryStatus,followup_history.created_at as followupHistoryCreatedAt,followup_history.updated_at as followHistoryupUpdated');
    $this->db->from('followup_history');
    $this->db->join('enquiry','enquiry.id = followup_history.enquriyID','left');
    $this->db->join('customers','customers.id = enquiry.clientID','left');
    $this->db->join('lead_status','lead_status.id = followup_history.lead_status','left');
    $this->db->join('users','users.id = followup_history.userID','left');
    $this->db->join('users as assigned','assigned.id = enquiry.assigned_by','left');
    $this->db->where($condition);
    if($this->session->userdata('user_type')!=2){
      if(!empty($usersID)){
        $this->db->where_in('followup_history.userID',$usersID );
      }
    }
	  return $this->db->get()->result();
    //echo $this->db->last_query();die;
  }


  
}