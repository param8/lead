<style>
.search_phone {
    display: block;
    position: absolute;
    background-color: #f7f7f7;
    padding: 12px 6px;
    border: 1px solid #d2d1d1;
    box-shadow: 0px 7px 0px 0px rgb(0 0 0 / 13%);
    max-height: 200px;
    border-radius: 7px;
    min-height: 180px;
    overflow-y: auto;
    border-bottom-left-radius: 5px;
    left: 24px;
    border-bottom-right-radius: 5px;
    z-index: 3;
    margin-top: 5px;
}
.search_phone p {
    background: white;
    padding: 5px;
    border-radius: 3px;
    margin-bottom: 5px;
}
.search_phone p a {
    color: dimgrey;
}
</style>
<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('Customer/update')?>" id="customerEditForm" method="post">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter Customer Detail</h4>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Phone No. <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$customer->phone?>" readonly>
                      <!-- <div class="row"><span class="search_phone col-lg-3 col-md-4 col-sm-6 col-12" id="customer_phone" style="display:none"></span></div> -->
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Full Name <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="name" id="name" value="<?=$customer->name?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Company Name </label>
                    <input type="text" class="form-control" name="company_name" id="company_name" value="<?=$customer->company_name?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Email </label>
                    <input type="email" class="form-control" name="email" id="email" value="<?=$customer->email?>">
                  </div>
                </div>

                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Address </label>
                    <textarea class="form-control" name="address" id="address"><?=$customer->address?></textarea>
                  </div>
                </div>


                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Alternate Contact No </label>
                    <input type="text" class="form-control" name="alternate_no" id="alternate_no" minlength="10"
                      maxlength="10"
                      oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$customer->alternate_no?>">
                  </div>
                </div>

                <div class="text-center">
                <button type="submit" class="btn btn-primary">Edit</button>
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
$("form#customerEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$customer->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('customers')?>";

        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});
</script>
<script>

function get_citys(stateID,customerID=NULL) {

$.ajax({
  url: '<?=base_url('Ajax_controller/get_city')?>',
  type: 'POST',
  data: {
    stateID,
    customerID
  },
  success: function(data) {
    $('.city').html(data);
  }
});
}

$( document ).ready(function() {
  get_citys(<?=$customer->state?>,<?=$customer->id?> );
});

</script>