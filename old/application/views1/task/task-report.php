<style>
#chart-container {
  font-family: Arial;
  height: 420px;
  Width: 100%;
  border: 2px dashed #aaa;
  border-radius: 5px;
  overflow: auto;
  text-align: center;
}

#github-link {
  position: fixed;
  top: 0px;
  right: 10px;
  font-size: 3em;
}
</style>
<link rel="stylesheet" href="<?=base_url('public/admin/assets/css/jquery.orgchart.css')?>">
<div class="main-wrapper">
  <div class="page-wrapper">
    <div class="content container-fluid">

      <div class="page-header">
        <div class="row">
          <div class="col-sm-6">
            <h3 class="page-title"><?=$page_title?></h3>
            <ul class="breadcrumb">
              <li><a href="<?=base_url('dashboard')?>">Dashboard/</a></li>
              <li class="breadcrumb-item"><a href="javascript:(0);"><?=$page_title?></a></li>
            </ul>
          </div>

        </div>
      </div>

      <div id="chart-container" style="text-align: left;"></div>


    </div>
  </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="<?=base_url('public/admin/assets/js/jquery.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('public/admin/assets/js/jquery.orgchart.js')?>"></script>
<script>
  (function($) {
  $(function() {
   var ds = {
     'name': '<?=$reports['product']?>',
     
     'children': [
      <?php foreach($users as $key=>$user){
        ?>
        { 'name': '<?=$user->name?>', 'title': '<?=$user->roleName?>',
          'children': [
            <?php 
               
              foreach($reports[$user->id]['object'] as $report){
                 $completed_work = json_decode($report['work_report_percentage'])->complete_work;
                $name = 'Create Date : '. date('d-m-Y',strtotime($report['created_at'])).'  Work Completed : '.$completed_work.'%<br> Work Time (in min) : '.$report['work_time'].' min';
                
              ?>
           { 'name': '<?=$name?>', 'title': '<?=$report['task_description']?>' },
           <?php } ?>
          ]
        },
      <?php } ?>
      
      ]
    };

    var oc = $('#chart-container').orgchart({
      'data' : ds,
      'nodeContent': 'title',
      'direction': 'l2r',
      'pan':true
    });

  });
})(jQuery);
</script>