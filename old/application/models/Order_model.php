<?php 
class Order_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  
  function make_query($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('courses', 'courses.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->group_by('items.orderID');
   $this->db->order_by('orders.id','desc');
  }
    
 function make_datatables($condition){
	  $this->make_query($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data($condition){
	  $this->make_query($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('courses', 'courses.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->group_by('items.orderID');
   $this->db->order_by('orders.id','desc');
	 return $this->db->count_all_results();
  }


  function make_query_test($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,test_series.title as courseName');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('test_series', 'test_series.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->group_by('items.orderID');
   $this->db->order_by('orders.id','desc');
  }
    
 function make_datatables_test($condition){
	  $this->make_query_test($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_test($condition){
	  $this->make_query_test($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  
  function get_all_data_test($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,test_series.title as courseName');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('test_series', 'test_series.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);

   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->group_by('items.orderID');
   $this->db->order_by('orders.id','desc');
	   return $this->db->count_all_results();
  }

public function store_order($data){
  $this->db->insert('orders', $data);
  return $this->db->insert_id();
}

public function store_item($data){
  return $this->db->insert('items', $data);
}

public function update_order($data, $id){
  $this->db->where('id', $id);
  return $this->db->update('orders', $data);
}

public function get_order($condition){
  $this->db->where($condition);
  return $this->db->get('orders')->row();
}

public function get_orders($condition){
  $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
  $this->db->from('orders');
  $this->db->join('items', 'orders.id=items.orderID','left');
  $this->db->join('courses', 'courses.id=items.courseID','left');
  $this->db->join('users', 'users.id=orders.userID','left');
  $this->db->where($condition);
  $this->db->group_by('items.orderID');
  $this->db->order_by('orders.id', 'desc');
  return $this->db->get()->result();
}


function make_query_booking($condition)
{
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('courses', 'courses.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);
    $this->db->group_by('items.orderID');
   
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('orders.id', 'desc');
 }
    
 function make_datatables_booking($condition){
	  $this->make_query_booking($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_booking($condition){
	  $this->make_query_booking($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }

  function get_all_data_booking($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('courses', 'courses.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);
    $this->db->group_by('items.orderID');
   
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('orders.id', 'desc');
	   return $this->db->count_all_results();
  }


  function make_query_booking_course($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('courses', 'courses.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);
    $this->db->group_by('items.orderID');
   
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('orders.id', 'desc');
  }
    
 function make_datatables_booking_course($condition){
	  $this->make_query_booking_course($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_booking_course($condition){
	  $this->make_query_booking_course($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_booking_course($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,courses.name as courseName,courses.image as coursePic');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('courses', 'courses.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);
    $this->db->group_by('items.orderID');
   
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->or_like('courses.name', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('orders.id', 'desc');
	   return $this->db->count_all_results();
  }


  function make_query_booking_test_series($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,test_series.title as courseName');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('test_series', 'test_series.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);
    $this->db->group_by('items.orderID');
   
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->or_like('test_series.title', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('orders.id', 'desc');
  }
    
 function make_datatables_test_series($condition){
	  $this->make_query_booking_test_series($condition,);
	  if($_POST["length"] != -1)
	  {
		  $this->db->limit($_POST['length'], $_POST['start']);
	  }
	  $query = $this->db->get();
	  return $query->result_array(); 
  // echo $this->db->last_query(); die;
  }

  function get_filtered_data_booking_test_series($condition){
	  $this->make_query_booking_test_series($condition);
	  $query = $this->db->get();
	  return $query->num_rows();
	  //echo $this->db->last_query();die;
  }
  function get_all_data_booking_test_series($condition)
  {
    $this->db->select('items.*,orders.orderID as order_id,orders.discount as discount,orders.gst as gst,orders.price as orderPrice,orders.total_price as total_price,users.name as userName,orders.payment_status as paymentStatus,orders.coupon_code as order_coupon_code,orders.status as orderStatus,test_series.title as courseName');
    $this->db->from('orders');
    $this->db->join('items', 'orders.id=items.orderID','left');
    $this->db->join('test_series', 'test_series.id=items.courseID','left');
    $this->db->join('users', 'users.id=orders.userID','left');
    $this->db->where($condition);
    $this->db->group_by('items.orderID');
   
   if(isset($_POST["search"]["value"]) && !empty($_POST["search"]["value"]))
   {
    $this->db->group_start();
    $this->db->like('users.name', $_POST["search"]["value"]);
    $this->db->or_like('orders.price', $_POST["search"]["value"]);
    $this->db->or_like('orders.discount', $_POST["search"]["value"]);
    $this->db->or_like('orders.coupon_code', $_POST["search"]["value"]);
    $this->db->or_like('test_series.title', $_POST["search"]["value"]);
    $this->db->group_end();
   }
   $this->db->order_by('orders.id', 'desc');
	   return $this->db->count_all_results();
  }



}
