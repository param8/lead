<div class="card">

  <form id="emailSettingAddForm" method="post" action="<?=base_url('setting/store_email_setting');?>">
    <div class="row col-md-12">
      <div class="col-md-6">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h5 class="card-title">Email Template Setting</h5>

        </div>
        <div class="card-body pt-0">
          <div class="settings-form">
            <div class="form-group form-placeholder">
              <label>Email Type <span class="star-red">*</span></label>
              <input type="text" class="form-control" name="type" value="">
            </div>
            <div class="form-group form-placeholder">
              <label>Assigin Email <span class="star-red">*</span></label>
              <select class="form-control js-example-basic-multiple" name="asigin_email" multiple="multiple">
                <option value="">Select User</option>
                <?php foreach($users as $users){?>
                <option value="<?=$users->email?>"><?=$users->name.'('.$users->email.')'?></option>
                <?php } ?>

              </select>
            </div>
            <div class="form-group form-placeholder">
              <label>Subject <span class="star-red">*</span></label>
              <input type="text" class="form-control" name="subject">
            </div>
            <div class="form-group mb-0">
              <div class="settings-btns">
                <button type="submit" class="btn btn-orange">Submit</button>
                <button type="submit" class="btn btn-grey">Cancel</button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="col-md-6">
        <div class="card-header d-flex justify-content-between align-items-center">
          <h5 class="card-title">SMTP</h5>
        </div>
        <div class="card-body pt-0">
          <div class="settings-form">
            <div class="form-group form-placeholder">
              <label>Email Templete <span class="star-red">*</span></label>
              <textarea  class="form-control" name="template" id="template"></textarea>
            </div>

          </div>
        </div>
      </div> -->
    </div>
  </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
$("form#emailSettingAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        //$('.modal').modal('hide');
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.reload();

        }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$('#template').summernote({
    height:400,
});
</script>