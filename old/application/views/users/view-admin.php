<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title"><?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active"><?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form class="card" action="<?=base_url('Admin/update')?>" id="userEditForm" method="post">
      <div class="row">
        <div class="col-md-6 col-lg-6 col-xl-6">
        <div class="">
            <div class="card-header">
              <h4 class="card-title">Basic Detail</h4>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label>Full Name <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="name" id="name" value="<?=$user->name?>" readonly>
              </div>

              

              <div class="form-group">
                <label>Email Address <span class="text-danger">*</span></label>
                <input type="email" class="form-control" name="email" id="email" value="<?=$user->email?>" readonly> 
              </div>


              <div class="form-group">
                <label>Contact No. <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="phone" id="phone" minlength="10" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->phone?>" readonly>
              </div>

              <div class="form-group">
                <label>Alternative No. </label>
                <input type="text" class="form-control" name="alternative_no" id="alternative_no" minlength="10" maxlength="10" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->user_alternative_no?>" readonly>
              </div>

              <div class="form-group">
                <label>User Role <span class="text-danger">*</span></label>
                <select class="form-control" name="user_type" id="user_type"
                  <?=$page_title== 'Admin' ? 'readonly' : ''?> readonly>
                  
                  <?php foreach($roles as $role){?>
                  <option value="<?=$role->id?>" <?=$page_title== $role->name ? 'selected' : ''?>><?=$role->name?>
                  </option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>Address <span class="text-danger">*</span></label>
                <textarea class="form-control" name="address" id="address" readonly><?=$user->address?></textarea>
              </div>
              <div class="form-group">
                <label>State <span class="text-danger">*</span></label>
                <select class="form-control js-example-basic-single" name="state" id="state" onchange="get_city(this.value)" disabled>
                  <option value="">Select State</option>
                  <?php foreach($states as $state){?>
                  <option value="<?=$state->id?>" <?=$state->id==$user->state ? 'selected' : ''?>><?=$state->name?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="form-group">
                <label>City <span class="text-danger">*</span></label>
                <select class="form-control city js-example-basic-single" name="city" id="city" disabled>
                  
                  <option value="">Select City</option>
                  
                </select>
              </div>
              <div class="form-group">
                <label>Pincode <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="pincode" id="pincode" minlength="6" maxlength="6" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->pincode?>" readonly>
              </div>

             
              
              
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-6 col-xl-6">
          <div class="">
            <div class="card-header">
              <h4 class="card-title">Provide Permission</h4>
            </div>
  
           
            <div class="card-body">
            <div class="form-group">
                <label>Gender <span class="text-danger">*</span></label><br>
                <?php $genders = array('Male','Female');
                  foreach($genders as $gender){
                  ?>
                 <span><input type="radio" class="" name="gender" value="<?=$gender?>" <?=$gender==$user->gender ? 'checked' : ''?> disabled> <?=$gender?></span>
                  <?php
                 }
                ?>
                <!-- <span><input type="radio" class="" name="gender" value="Male" checked> Male</span>
                <span class="ml-5"><input type="radio" class="" name="gender" value="Female"> Female</span> -->
              </div>
            <div class="form-group">
                <label>User Permission <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="user_permission" id="user_permission"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->user_permission?>" readonly>
              </div>

              <div class="form-group">
                <label>Lead Permission <span class="text-danger">*</span></label>
                <input type="text" class="form-control" name="lead_permission" id="lead_permission"  oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?=$user->lead_permission?>" readonly>
              </div>
              <div class="table-responsive">
                <table class=" table table-hover table-center mb-0">
                  <thead>
                    <tr>
                      <th>Menu</th>
                      <th>View</th>
                      <th>Add</th>
                      <th>Edit</th>
                      <th>Delete</th>
                      <th>Upload</th>
                      <th>Export</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    // print_r($menus);
                    foreach($menues as $menu){
                      $menuID = $menu['id'];
                      $permission = $user->permission;
                      $permission = json_decode($permission)->$menuID;
                      if($menu['parent_menu']!='Dashboard'){
                        //print_r( $permission);die;  
                      ?>
                    <tr>
                      <td><?=$menu['parent_menu']?></td>
                      <td><input type="checkbox" name="view[<?=$menu['id']?>]" <?=in_array('View',$permission) ? 'checked' : ''?> value="View" disabled></td>
                      <td><input type="checkbox" name="add[<?=$menu['id']?>]" <?=in_array('Add',$permission) ? 'checked' : ''?> value="Add" disabled></td>
                      <td><input type="checkbox" name="edit[<?=$menu['id']?>]" <?=in_array('Edit',$permission) ? 'checked' : ''?> value="Edit" disabled></td>
                      <td><input type="checkbox" name="delete[<?=$menu['id']?>]" <?=in_array('Delete',$permission) ? 'checked' : ''?> value="Delete" disabled></td>
                      <td><input type="checkbox" name="upload[<?=$menu['id']?>]" <?=in_array('Upload',$permission) ? 'checked' : ''?> value="Upload" disabled></td>
                      <td><input type="checkbox" name="export[<?=$menu['id']?>]" <?=in_array('Export',$permission) ? 'checked' : ''?> value="Export" disabled></td>
                    </tr>
                    <?php } } ?>
                  </tbody>
                </table>
              </div>


            </div>
          </div>
        </div>
        <div class="col-md-12">
           <div class="">
              <div class="card-header">
                <h4 class="card-title">Website Basic Details</h4>
              </div>
              <div class="card-body pt-0">
             <div class="settings-form row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Website Name <span class="star-red">*</span></label>
                    <input type="hidden" name="adminID" value="1">
                    <input class="form-control" value="<?=$website_setting->site_name?>" readonly>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Email <span class="star-red">*</span></label>
                    <input class="form-control" value="<?=$website_setting->site_email?>" readonly>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Contact Number <span class="star-red">*</span></label>
                    <input class="form-control" value="<?=$website_setting->site_contact?>" readonly>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Whatsapp Number <span class="star-red">*</span></label>
                    <input class="form-control" value="<?=$website_setting->whatsapp_no?>" readonly>
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Footer Content <span class="star-red">*</span></label>
                        <input class="form-control" value="<?=$website_setting->footer_contant?>" readonly>
                    </div>
                  </div>  
                <div class="col-md-6">
                  <div class="form-group">
                    <p class="settings-label">Logo <span class="star-red">*</span></p>
                      <div class="upload-images">
                        <img src="<?=base_url($website_setting->site_logo)?>" alt="Image">
                        
                      </div>      
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                        <label>Description <span class="star-red">*</span></label>
                        <textarea class="form-control" readonly><?=$website_setting->discription?></textarea>
                    </div>
                  </div>
                             
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Address </label>
                    <textarea class="form-control" readonly><?=$website_setting->site_address?></textarea>
                  </div>
                </div>                
                
              </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
$("form#userEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("id", '<?=$user->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 5000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        setTimeout(function() {

          location.href = "<?=base_url('edit-admin/'.base64_encode($user->id))?>";

        }, 1000)

      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

$( document ).ready(function() {
  get_city(<?=$user->state?>,<?=$user->id?> );
});
</script>