<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Create <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Create <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('Task/store')?>" id="taskAddForm" method="post">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter <?=$page_title?> Detail</h4>
            </div>
            <div class="card-body">
              <div class="row">

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Product</label>
                    <textarea class="form-control" col-row="2" name="product" id="product" placeholder="Product"
                      readonly><?=$product_details?></textarea>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Client Detail</label>
                    <input type="text" class="form-control" name="client_detail" id="client_detail"
                      value="<?=$sale->customerName.'('.$sale->customerPhone.'/'.$sale->company_name.') '?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Role </label>
                    <select class="form-control js-example-basic-multiple" name="role" id="role"
                      onchange="get_user(this.value)">
                      <option value="">Select Role</option>
                      <option value="0">Desigin+Development</option>
                      <?php
                    foreach($roles as $role){
                    $user_task_permission = json_decode($role->other_permission);
                      $task_permission =  $user_task_permission->task_developer;
                    if($task_permission==1){
                  ?>
                      <option value="<?=$role->id?>">
                        <?=$role->name?></option>
                      <?php } } ?>

                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>User </label>
                    <select class="form-control js-example-basic-multiple" name="assign_to[]" id="user"
                      multiple="multiple">
                      <option value="">Select User</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Priority <span class="text-danger">*</span></label>
                    <?php $priorities = array('Low','Normal','High','Immediate');?>
                    <select class="form-control js-example-basic-multiple" name="priority" id="priority">
                      <option value="">Select Priority</option>
                      <?php
                       foreach($priorities as $priority){
                      ?>
                      <option value="<?=$priority?>"><?=$priority?></option>
                      <?php } ?>

                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Assign Date <span class="text-danger">*</span></label>
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="assigin_date"
                      id="assigin_date" value="<?=date('d-m-Y')?>" placeholder="Assign Date" disabled>
                  </div>
                </div>

                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Requirement</label>
                    <textarea class="form-control" name="requirement" id="requirement"
                      placeholder="Requirement"></textarea>
                  </div>
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
$("form#taskAddForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("saleID", '<?=$sale->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 10000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        ajaxSendMail(data.assign_to, data.assign_by, data.saleID)
        // setTimeout(function() {
        //   url = "<?//=base_url('task')?>";
        //   location.href = url;
        // }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function ajaxSendMail(assign_to, assign_by, saleID) {
  $.ajax({
    url: "<?=base_url('Send_mail/createTaskSendMail')?>",
    type: 'POST',
    data: {
      assign_to,
      assign_by,
      saleID
    },
    success: function(data) {
      toastNotif({
        text: 'Mail sent successfully',
        color: '#5bc83f',
        timeout: 10000,
        icon: 'valid'
      });
      setTimeout(function() {
        url = "<?=base_url('task')?>";
        location.href = url;
      }, 1000)

    },

  });
}


$('#requirement').summernote({
  height: 300,
});

$('#work_progress').summernote({
  height: 300,
});
</script>
<script>
function get_work_percentage(val) {
  var work = $('#work').val();

  if (val != '') {
    var percentage = (val * 100) / work;
    $('#complete_work').val(work - percentage);
  } else {

  }
}

function get_user(id) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_users')?>',
    type: 'post',
    data: {
      id
    },
    dataType: 'html',
    success: function(response) {
      $('#user').html(response);

    }
  });
}
</script>