<div class="page-wrapper">
  <div class="content container-fluid">
    <div class="page-header">
      <div class="row">
        <div class="col">
          <h3 class="page-title">Edit <?=$page_title?></h3>
          <ul class="breadcrumb">
            <li class=""><a href="<?=base_url('dashboard')?>">Dashboard</a></li>/
            <li class="breadcrumb-item active">Edit <?=$page_title?></li>
          </ul>
        </div>
      </div>
    </div>
    <form action="<?=base_url('Task/update')?>" id="taskEditForm" method="post">
      <div class="row">
        <div class="col-md-12 col-lg-12 col-xl-12">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Enter <?=$page_title?> Detail</h4>
            </div>
            <div class="card-body">
              <div class="row">

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Product</label>
                    <textarea class="form-control" col-row="2" name="product" id="product" placeholder="Product"
                      readonly><?=$task->product?></textarea>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Client Detail</label>
                    <input type="text" class="form-control" name="client_detail" id="client_detail"
                      value="<?=$sale->customerName.'('.$sale->customerPhone.'/'.$sale->company_name.') '?>" readonly>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Role </label>
                    <select class="form-control js-example-basic-multiple" name="role" id="role"
                      onchange="get_user(this.value)">
                      <option value="">Select Role</option>
                      <option value="0" <?=$task->role==0 ? 'selected' : ''?>>Desigin+Development</option>
                      <?php
                    foreach($roles as $role){
                    $user_task_permission = json_decode($role->other_permission);
                      $task_permission =  $user_task_permission->task_developer;
                    if($task_permission==1){
                  ?>
                      <option value="<?=$role->id?>" <?= $task->role==$role->id ? 'selected' : ''?>>
                        <?=$role->name?></option>
                      <?php } } ?>

                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>User </label>
                    <select class="form-control js-example-basic-multiple" name="assign_to[]" id="user"
                      multiple="multiple">
                      <option value="">Select User</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Priority <span class="text-danger">*</span></label>
                    <?php $priorities = array('Low','Normal','High','Immediate');?>
                    <select class="form-control js-example-basic-multiple" name="priority" id="priority">
                      <option value="">Select Priority</option>
                      <?php
                       foreach($priorities as $priority){
                      ?>
                      <option value="<?=$priority?>" <?= $task->priority==$priority ? 'selected' : ''?>><?=$priority?>
                      </option>
                      <?php } ?>

                    </select>
                  </div>
                </div>


                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Work Status <span class="text-danger">*</span></label>
                    <?php $work_started = array('Not Started','Work In Progress','Completed');?>
                    <select class="form-control js-example-basic-multiple" name="work_status" id="work_status">
                      <option value="">Select Work Status</option>
                      <?php
                       foreach($work_started as $workStart){
                      ?>
                      <option value="<?=$workStart?>" <?= $task->work_status==$workStart ? 'selected' : ''?>>
                        <?=$workStart?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Start Date </label>
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="start_date"
                      id="start_date" placeholder="Start Date"
                      value="<?=!empty($task->start_date) ? date('d-m-Y',strtotime($task->start_date)) : ''?>">
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>End Date </label>
                    <input type="text" onfocus="(this.type='date')" class="form-control" name="end_date" id="end_date"
                      placeholder="End Date"
                      value="<?=!empty($task->end_date) ? date('d-m-Y',strtotime($task->end_date)) : ''?>">
                  </div>
                </div>

                <!-- <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Work estimates and progress </label>
                    <div class="row">
                      <div class="col-md-4 col-lg-4 col-xl-4">
                      <label>Work</label>
                        <input type="text" class="form-control" name="work" id="work"
                          oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                          value="100" readonly>
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                      <label>Remaining work</label>
                      <?php 
                        // $work_percentage = json_decode($task->work_percentage);
                        // $remaning_work = $work_percentage->remaning_work;
                        // $complete_work = $work_percentage->complete_work;
                      ?>
                        <input type="text" class="form-control" name="remaning_work" id="remaning_work"
                          oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');"
                           onkeyup="get_work_percentage(this.value)" value="<?//=$remaning_work?>">
                      </div>
                      <div class="col-md-4 col-lg-4 col-xl-4">
                      <label>% Complete</label>
                      <input type="text" class="form-control" name="complete_work" id="complete_work"
                          oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" value="<?//=$complete_work?>"
                           readonly>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Tools <span class="text-danger">*</span></label><br>
                    <?php $tools = array('HTML','CSS','Bootstrap','PHP','Codeigniter','Laravel','UI Design','Wordpress','SEO','SMO','SMM','PCC');
                      foreach($tools as $tool){
                    ?>
                    <span class="p-2"><input type="checkbox" name="tools[]"
                        <?=  (role_other_permission() != 1 OR $this->session->userdata('user_type')==2)? 'disabled' : ''?>
                        <?=in_array($tool,explode(',',$task->tools)) ? 'checked' : ''?> value="<?=$tool?>"> <span
                        class="m-1"><?=$tool?></span></span>
                    <?php 
                      }
                    ?>
                  </div>
                </div>

                <hr>

                <div class="col-md-12 col-lg-12 col-xl-12">
                  <div class="form-group">
                    <label>Work Requirement <span class="text-danger">*</span></label>
                    <textarea class="form-control" name="requirement"
                      id="requirement"><?=$task->requirement?></textarea>
                  </div>
                </div>

                <?php if($this->session->userdata('user_type')==2){?>
                <div class="col-md-6 col-lg-6 col-xl-6">
                  <div class="form-group">
                    <label>Task Approve/Disapprove <span class="text-danger">*</span></label><br>
                    <?php $task_status = array(1 =>'Approve', 2 =>'Disapprove'); 
                      foreach($task_status as $key=>$statu){
                    ?>
                    <span class="p-2"><input type="radio" name="task_status" onclick="check_task_status(this.value)"
                        <?=($key==$task->status) ? 'checked' : ''?> <?=($task->status==1) ? 'disabled' : ''?>
                        value="<?=$key?>"> <span class="m-1"><b><?=$statu?></b></span></span>
                    <?php 
                      }
                    ?>
                  </div>
                </div>

                <div class="col-md-6 col-lg-6 col-xl-6" id="reason_div"
                  <?=($task->status!=2) ? 'style="display:none" ' : ''?>>
                  <div class="form-group">
                    <label>Disapprove Reason </label>
                    <textarea class="form-control" name="reason" id="reason"><?=$task->reason?></textarea>
                  </div>
                </div>
                <?php } ?>


                <div class="text-center">
                  <button type="submit" class="btn btn-outline-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
</div>
<script>
$("form#taskEditForm").submit(function(e) {
  $(':input[type="submit"]').prop('disabled', true);
  e.preventDefault();
  var formData = new FormData(this);
  formData.append("saleID", '<?=$task->saleID?>');
  formData.append("taskID", '<?=$task->id?>');
  $.ajax({
    url: $(this).attr('action'),
    type: 'POST',
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data) {
      if (data.status == 200) {
        toastNotif({
          text: data.message,
          color: '#5bc83f',
          timeout: 10000,
          icon: 'valid'
        });
        $(':input[type="submit"]').prop('disabled', false);
        ajaxSendMail(data.taskID, data.assign_to, data.task_status)
        // setTimeout(function() {

        //   url = "<?//=$type == 'sale' ? base_url('sales') : base_url('task')?>";
        //   location.href = url;
        // }, 1000)
      } else if (data.status == 403) {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });

        $(':input[type="submit"]').prop('disabled', false);
      } else {
        toastNotif({
          text: data.message,
          color: '#da4848',
          timeout: 5000,
          icon: 'error'
        });
        $(':input[type="submit"]').prop('disabled', false);
      }
    },
    error: function() {}
  });
});

function ajaxSendMail(taskID, assign_to, task_status) {
  $.ajax({
    url: "<?=base_url('Send_mail/taskUpdateSendMail')?>",
    type: 'POST',
    data: {
      taskID,
      assign_to,
      task_status
    },
    success: function(data) {
      toastNotif({
        text: 'Mail sent successfully',
        color: '#5bc83f',
        timeout: 10000,
        icon: 'valid'
      });
      setTimeout(function() {
        url = "<?=base_url('task')?>";
        location.href = url;
      }, 1000)
    },
  });
}


function get_user(id, assign_to) {
  $.ajax({
    url: '<?=base_url('Ajax_controller/get_users')?>',
    type: 'post',
    data: {
      id,
      assign_to
    },
    dataType: 'html',
    success: function(response) {
      $('#user').html(response);

    }
  });
}

$(document).ready(function() {
  get_user(<?=$task->role?>, '<?=$task->assign_to?>')
});
</script>
<?php if(($this->session->userdata('user_type') == 2 OR $this->session->userdata('id') == $task->assign_by)){?>
<script type="text/javascript">
$('#requirement').summernote({
  height: 300,
});
</script>
<?php }else{?>
<script type="text/javascript">
$('#requirement').summernote({
  height: 300,

});
$("#requirement").summernote("disable");
</script>
<?php } ?>
<script>
function get_work_percentage(val) {
  var work = $('#work').val();

  if (val != '') {
    var percentage = (val * 100) / work;
    $('#complete_work').val(work - percentage);
  } else {

  }
}

function check_task_status(val) {

  if (val == 2) {
    $('#reason_div').show();

  } else {
    $('#reason_div').hide();
  }

}
</script>