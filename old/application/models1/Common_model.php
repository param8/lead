<?php 
class Common_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

  public function get_site_info($condition){
    $this->db->where($condition);
    return $this->db->get('site_info')->row();
  }
 


  public function get_country()
  {
    return $this->db->get('countries')->result();
  }

  public function get_country_by_id($condition)
  {
    $this->db->where($condition);
    return $this->db->get('countries')->row();
  }

  public function get_states($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('states')->result();
  }

  public function get_city($condition)
  {
      $this->db->where($condition);
      $this->db->order_by('name', 'ASC');
      return $this->db->get('cities')->result();
  }

  public function get_state_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('states')->row();
  }

  public function get_city_id($condition)
  {
      $this->db->where($condition);
      return $this->db->get('cities')->row();
  }
  public function get_country_id($condition){
    $this->db->where($condition);
      return $this->db->get('countries')->row();
  }

  public function store_city($city_data){
  $this->db->insert('cities', $city_data);
  return $this->db->insert_id();
}

public function store_state($state_data){
  $this->db->insert('states', $state_data);
  return $this->db->insert_id();
}

public function get_permission($condition){
  $this->db->where($condition);
  return $this->db->get('permission')->row();
}

public function store_order($data){
  $this->db->insert('orders', $data);
  return $this->db->insert_id();
}

public function store_item($data){
  return $this->db->insert('items', $data);
}

public function get_order($condition){
  $this->db->where($condition);
  return $this->db->get('orders')->row();
}

public function get_menues($condition){
  $this->db->where($condition);
  $this->db->order_by('ordering','asc');
  return $this->db->get('menues')->result_array();
}

public function get_menu($condition){
  $this->db->where($condition);
  return $this->db->get('menues')->row();
} 


}
